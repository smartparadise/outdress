//
//  Router.swift
//  EventApp
//
//  Created by Pankaj Sharma on 29/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import Foundation
import UIKit

class Router{
    
    public static let shared = Router()
    var window:UIWindow?
    
    func makeRootController(){
        let delegate = UIApplication.shared.delegate as! AppDelegate
        window = delegate.window
        if Utility.isKeyPresentInUserDefaults(key: UserDefaultsKeys.firstTime.rawValue){
                if UserDefaults.standard.isFirstTime(){
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "TourVC") as! TourVC
                    let navVC = UINavigationController(rootViewController: vc)
                    navVC.isNavigationBarHidden = true
                    window?.rootViewController = navVC
                    window?.makeKeyAndVisible()
                }else{
                    if Utility.isKeyPresentInUserDefaults(key: UserDefaultsKeys.isLoggedIn.rawValue){
                        if UserDefaults.standard.isLoggedIn(){
                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "HomeTabBar") as! HomeTabBar
                            let navVC = UINavigationController(rootViewController: vc)
                            navVC.isNavigationBarHidden = true
                            window?.rootViewController = navVC
                            window?.makeKeyAndVisible()
                        }else{
                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "LoginVC") as! LoginVC
                            let navVC = UINavigationController(rootViewController: vc)
                            navVC.isNavigationBarHidden = true
                            window?.rootViewController = navVC
                            window?.makeKeyAndVisible()
                        }
                    }else{
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "LoginVC") as! LoginVC
                        let navVC = UINavigationController(rootViewController: vc)
                        navVC.isNavigationBarHidden = true
                        window?.rootViewController = navVC
                        window?.makeKeyAndVisible()
                    }
                    
                }
            }else{
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "TourVC") as! TourVC
                               let navVC = UINavigationController(rootViewController: vc)
                               navVC.isNavigationBarHidden = true
                               window?.rootViewController = navVC
                               window?.makeKeyAndVisible()
            }
    }
}
