//
//  UDManager.swift
//  EventApp
//
//  Created by Pankaj Sharma on 28/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import Foundation
import UIKit


class JSONHelper<T: Codable> {
  
  init() {
    
  }
  
  func getCodableModel(data: Data) -> T? {
    let model = try? JSONDecoder().decode(T.self, from: data)
    return model
  }
  
  func getData(model: T) -> Data? {
    guard let data: Data = try? JSONEncoder().encode(model) else {
      return nil
    }
    return data
  }
}



class UserPreferences{
    let DEFAULT_KEY = "EVENT_APP_KEY"
    
    static let shared = UserPreferences()
    
    var user:UserData?{
        get {
            return fetchData()
        }set{
            if let value = newValue{
                saveData(value)
            }else{
                removeData()
            }
        }
    }
    
    var userOutfits:[UserOutfits]?{
        get{
            return fetchOutfits()
        }set{
            if let value = newValue{
                saveOutfits(value)
            }else{
                removeOutfits()
            }
        }
    }
    
    func saveOutfits(_ value:[UserOutfits]){
        do{
            let data = try NSKeyedArchiver.archivedData(withRootObject: value, requiringSecureCoding: false)
                   UserDefaults.standard.set(data, forKey: "Outfits")
        }catch{
            print("Can't store")
        }
       
    }
    func fetchOutfits()->[UserOutfits]?{
        if let data = UserDefaults.standard.data(forKey: "Outfits"){
            do{
                let model = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [UserOutfits]
                               return model
            }catch{
                print("Cannot load")
            }
               
            }
       return nil
    }
    
    func removeOutfits(){
        UserDefaults.standard.removeObject(forKey: "Outfits")
    }

    func saveData(_ value:UserData){
        do{
            let data = try NSKeyedArchiver.archivedData(withRootObject: value, requiringSecureCoding: false)
                   UserDefaults.standard.set(data, forKey: DEFAULT_KEY)
        }catch{
            print("Can't store")
        }
       
    }
    
    func fetchData()->UserData?{
        if let data = UserDefaults.standard.data(forKey: DEFAULT_KEY){
            do{
                let model = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? UserData
                               return model
            }catch{
                print("Cannot load")
            }
               
            }
       return nil
    }
    
    func removeData(){
        UserDefaults.standard.removeObject(forKey: DEFAULT_KEY)
    }
}


//MARK: - User Defaults Extension
public enum UserDefaultsKeys:String{
    case userId
    case sessionId
    case isLoggedIn
    case stayLogged
    case username
    case password
    case deviceToken
    case profilePic
    case profileFor
    case address
    case shouldShow
    case notification
    case firstTime
    case calendarFirstTime
    case loginType
    case isDarkMap
    case email
}

extension UserDefaults{
    
    func setUserId(userId:String?){
        set(userId, forKey: UserDefaultsKeys.userId.rawValue)
    }
    
    func getUserId()->String{
        return value(forKey: UserDefaultsKeys.userId.rawValue) as! String
    }
    
    func setEmail(userId:String?){
           set(userId, forKey: UserDefaultsKeys.email.rawValue)
       }
       
       func getEmail()->String{
           return value(forKey: UserDefaultsKeys.email.rawValue) as! String
       }
    
    func setSessionId(sessionId:String?){
        set(sessionId, forKey: UserDefaultsKeys.sessionId.rawValue)
    }
    
    func removeSessionId(){
        removeObject(forKey: UserDefaultsKeys.sessionId.rawValue)
    }
    
    func removeUserId(){
        removeObject(forKey: UserDefaultsKeys.userId.rawValue)
    }
    
    func getSessionId()->String{
        return value(forKey: UserDefaultsKeys.sessionId.rawValue) as! String
    }
    func setUsername(username:String){
        set(username, forKey: UserDefaultsKeys.username.rawValue)
    }
    
    func getUsername()->String{
        return value(forKey: UserDefaultsKeys.username.rawValue) as! String
    }
    
    func setPassword(password:String){
        set(password, forKey: UserDefaultsKeys.password.rawValue)
    }
    
    func getPassword()->String{
        return value(forKey: UserDefaultsKeys.password.rawValue) as! String
    }
    
    func setDeviceToken(token:String){
        set(token, forKey: UserDefaultsKeys.deviceToken.rawValue)
    }
    
    func getDeviceToken()->String{
        return value(forKey: UserDefaultsKeys.deviceToken.rawValue) as! String
    }
    
    func setLoggedIn(logged:Bool){
        set(logged, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    func isLoggedIn()->Bool{
        return value(forKey: UserDefaultsKeys.isLoggedIn.rawValue) as! Bool
    }
    
    func setAddress(address:String){
        set(address, forKey: UserDefaultsKeys.address.rawValue)
    }
    
    func getAddress()->String{
        return value(forKey: UserDefaultsKeys.address.rawValue) as! String
    }
    
    func setStayLogged(active:Bool){
        set(active, forKey: UserDefaultsKeys.stayLogged.rawValue)
    }
    
    func isStayLoggedActive() -> Bool{
        return value(forKey: UserDefaultsKeys.stayLogged.rawValue) as! Bool
    }
    
    func setProiflePic(image:String){
        set(image, forKey: UserDefaultsKeys.profilePic.rawValue)
    }
    
    func getProfilePic() -> String{
        return value(forKey: UserDefaultsKeys.profilePic.rawValue) as! String
    }
    
    func setProfileFor(profileFor:String){
        set(profileFor,forKey: UserDefaultsKeys.profileFor.rawValue)
    }
    
    func getProfileFor()->String{
        return value(forKey: UserDefaultsKeys.profileFor.rawValue) as! String
    }
    
    func setNotification(enabled:String){
        set(enabled, forKey: UserDefaultsKeys.notification.rawValue)
    }
    
    func getNotification()->String{
        return value(forKey: UserDefaultsKeys.notification.rawValue) as! String
    }
    
    func setShowMe(enabled:String){
        set(enabled, forKey:UserDefaultsKeys.shouldShow.rawValue)
    }
    
    func getShowMe()->String{
        return value(forKey: UserDefaultsKeys.shouldShow.rawValue) as! String
    }
    
    func setLoginType(type:String){
        set(type, forKey: UserDefaultsKeys.loginType.rawValue)
    }
    
    func getLoginType()->String{
        return value(forKey: UserDefaultsKeys.loginType.rawValue) as! String
    }
    
    func setFirstTime(isFirst:Bool){
        set(isFirst, forKey: UserDefaultsKeys.firstTime.rawValue)
    }
    
    func isFirstTime() -> Bool{
        return value(forKey: UserDefaultsKeys.firstTime.rawValue) as! Bool
    }
    
    func setCalendarFirstTime(isFirst:Bool){
        set(isFirst, forKey: UserDefaultsKeys.calendarFirstTime.rawValue)
    }
    
    func isCalendarFirstTime() -> Bool{
        return value(forKey: UserDefaultsKeys.calendarFirstTime.rawValue) as! Bool
    }
    
    func setDarkMap(isDark:Bool){
        set(isDark, forKey: UserDefaultsKeys.isDarkMap.rawValue)
    }
    
    func isDarkMap() -> Bool{
        return value(forKey: UserDefaultsKeys.isDarkMap.rawValue) as! Bool
    }
}


//MARK: - Data Extension to append String
extension Data {
    mutating func append(string: String) {
        let data = string.data(
            using: String.Encoding.utf8,
            allowLossyConversion: true)
        append(data!)
    }
}

extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
            }
            .joined(separator: "&")
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}

extension UIImage {
    // MARK: - UIImage+Resize
    func compressTo(_ expectedSizeInMb:Double) -> Data? {
        let sizeInBytes = Int(expectedSizeInMb * 1024 * 1024)
        var needCompress:Bool = true
        var imgData:Data?
        print(self)
        var compressingValue:CGFloat = 1.0
        while (needCompress && compressingValue > 0.0) {
            if let data:Data = self.jpegData(compressionQuality: compressingValue){
                if data.count < sizeInBytes {
                    needCompress = false
                    imgData = data
                } else {
                    
                    compressingValue -= 0.1
                }
            }
          
        }
        
        
        if let data = imgData {
            if (data.count < sizeInBytes) {
                
                print(data.count)
                return data
            }
        }
        return nil
    }
    
        func resized() -> UIImage? {
            print("width",size.width)
            print("height",size.height)
            if size.width > 1024{
                let ratio = size.width/size.height
                let widthRatio = 1024/size.width
                let width = size.width * widthRatio
                let height = width/ratio
                let canvasSize = CGSize(width: width, height: height)
                UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
                defer { UIGraphicsEndImageContext() }
                draw(in: CGRect(origin: .zero, size: canvasSize))
                return UIGraphicsGetImageFromCurrentImageContext()
            }else{
                return self
            }
        }
    
        func resized(toWidth width: CGFloat) -> UIImage? {
            let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
            UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
            defer { UIGraphicsEndImageContext() }
            draw(in: CGRect(origin: .zero, size: canvasSize))
            return UIGraphicsGetImageFromCurrentImageContext()
        }
}

extension UICollectionView {
    func scrollToNextItem() {
        let contentOffset = CGFloat(floor(self.contentOffset.x + self.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }
    
    func scrollToPreviousItem() {
        let contentOffset = CGFloat(floor(self.contentOffset.x - self.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }
    
    func moveToFrame(contentOffset : CGFloat) {
        self.setContentOffset(CGPoint(x: contentOffset, y: self.contentOffset.y), animated: true)
    }
}
