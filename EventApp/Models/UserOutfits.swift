//
//  UserOutfits.swift
//  EventApp
//
//  Created by Pankaj Sharma on 28/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import Foundation
import ObjectMapper

class UserOutfitDetails:Mappable{
    
    var status:Int?
    var message:String?
    var metadata:Metadata?
    
    var outfitData:[UserOutfits]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        message <- map["message"]
        metadata <- map["_metadata"]
        outfitData <- map["data"]
    }
}

class UserOutfits:Mappable{
    
    var id:String?
    var userId:String?
    var type:String?
    var name:String?
    var color:String?
    var picture:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        userId <- map["user_id"]
        type <- map["type"]
        name <- map["name"]
        color <- map["color"]
        picture <- map["picture"]
    }
}

