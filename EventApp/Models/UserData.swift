//
//  UserData.swift
//  EventApp
//
//  Created by Pankaj Sharma on 28/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import Foundation
import ObjectMapper

class Login:Mappable{
    var status:Int?
    var data:UserData?
    var method:String?
    var message:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        data <- map["data"]
        message <- map["message"]
        method <- map["method"]
    }
    
}

class UserData:Mappable{
    var id:String?
    var sessionId:String?
    var firstName:String?
    var lastName:String?
    var email:String?
    var country:String?
    var phone:String?
    var gender:String?
    var address:String?
    var city:String?
    var state:String?
    var latitude:String?
    var longitude:String?
    var pincode:String?
    var isEmailVerified:String?
    var profilePic:String?
    var userType:String?
    var isPhoneVerified:String?
    var accountStatus:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        sessionId <- map["session_id"]
        firstName <- map["first_name"]
        lastName <- map["last_name"]
        email <- map["email"]
        isEmailVerified <- map["is_email_verified"]
        country <- map["country"]
        phone <- map["phone_number"]
        gender <- map["gender"]
        address <- map["address"]
        city <- map["city"]
        state <- map["state"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        pincode <- map["pincode"]
        isPhoneVerified <- map["is_phone_verified"]
        profilePic <- map["profile_picture"]
        userType <- map["user_type"]
        accountStatus <- map["account_status"]
        
    }
}

