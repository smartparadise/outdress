//
//  EventDetails.swift
//  EventApp
//
//  Created by Pankaj Sharma on 28/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import Foundation
import ObjectMapper

class EventDetails:Mappable{
    
    var status:Int?
    var message:String?
    var metadata:Metadata?
    
    var eventDetails:[UserEvents]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        message <- map["message"]
        metadata <- map["_metadata"]
        eventDetails <- map["data"]
    }
}

class UserEvents:Mappable{
    var category:String?
    var created_at:String?
    var end_datetime:String?
    var id:Int?
    var outfits:String?
    var start_datetime:String?
    var title:String?
    var updated_at:String?
    var user_id:Int?
   
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        category <- map["category"]
        created_at <- map["created_at"]
        end_datetime <- map["end_datetime"]
        id <- map["id"]
        outfits <- map["outfits"]
        start_datetime <- map["start_datetime"]
        title <- map["title"]
        updated_at <- map["updated_at"]
        user_id <- map["user_id"]
    }
}

class Outfits:Mappable{
    var upper:UserOutfits?
    var lower:UserOutfits?
    required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
         upper <- map["upper"]
         lower <- map["lower"]
           
       }
}
