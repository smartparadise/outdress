//
//  AllOutfits.swift
//  EventApp
//
//  Created by Pankaj Sharma on 28/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import Foundation
import ObjectMapper

class Metadata:Mappable{
    var page:Int?
    var limit:Int?
    var remainingPages:Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        page <- map["page"]
        limit <- map["limit"]
        remainingPages <- map["remaining_pages"]
    }
}

class OutfitDetails:Mappable{
    
    var status:Int?
    var message:String?
    var metadata:Metadata?
    
    var outfitData:[OutfitData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        message <- map["message"]
        metadata <- map["_metadata"]
        outfitData <- map["data"]
    }
}

class OutfitData:Mappable{
    
    var id:String?
    var name:String?
    var weather:String?
    var event:String?
    var outfitFor:String?
    var outfitCategory:String?
    var image:String?
    var status:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        weather <- map["weather"]
        event <- map["event"]
        outfitFor <- map["outfit_for"]
        outfitCategory <- map["outfit_category"]
        image <- map["image"]
        status <- map["status"]
    }
}
