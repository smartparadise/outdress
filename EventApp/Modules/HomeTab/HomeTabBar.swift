//
//  HomeTabBar.swift
//  EventApp
//
//  Created by Pankaj Sharma on 22/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit

class HomeTabBar: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        selectedIndex = 2
    }

}
