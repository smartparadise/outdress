//
//  ClosetVC.swift
//  EventApp
//
//  Created by Pankaj Sharma on 22/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit

class ClosetVC: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var closetTable: UITableView!
    var userOutfits = [UserOutfits]()
    
    let closetArray = ["Jeans","Shirts","Trousers","Shoes","T-Shirts",
    "Clutches","Shoes","T-Shirts","Clutches"]
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        closetTable.register(UINib(nibName: "ClosetCell", bundle: nil), forCellReuseIdentifier: "ClosetCell")
        closetTable.delegate = self
        closetTable.dataSource = self
        fetchOutdress(showLoader:true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchOutdress(showLoader:false)
    }
    
    @IBAction func addClosetTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "AddOwnOutitVC") as! AddOwnOutitVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func fetchOutdress(showLoader:Bool){
        if showLoader{
            Utility.showLoading(vc: self)
        }
        
        NetworkManager().getUserOutfit(page: "1", limit: "20") { (error, userOutfits) in
            DispatchQueue.main.async {
                Utility.removeLoading(vc: self)
                if error == nil{
                    self.userOutfits = userOutfits ?? []
                    
                    self.closetTable.reloadData()
                }else{
                    let alert = Utility.makeAlert(title: "", message: error!)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
}

extension ClosetVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userOutfits.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClosetCell") as! ClosetCell
        cell.titleLabel.text = userOutfits[indexPath.row].name
        
        let delTap = UITapGestureRecognizer(target: self, action: #selector(delTapped))
        cell.deleteButton.isUserInteractionEnabled = true
        cell.deleteButton.addGestureRecognizer(delTap)
        let editTap = UITapGestureRecognizer(target: self, action: #selector(editTapped))
        cell.editButton.isUserInteractionEnabled = true
        cell.editButton.addGestureRecognizer(editTap)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    @objc func delTapped(_ sender:UITapGestureRecognizer){
        let point = sender.location(in: self.closetTable)
        let index = self.closetTable.indexPathForRow(at: point)
        let id = userOutfits[index!.row].id
        let alert = UIAlertController(title: "", message: "Are you sure to delete this closet?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (action) in
            self.deleteCloset(id:id!)
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true , completion: nil)
    }
    
    @objc func editTapped(_ sender:UITapGestureRecognizer){
        let point = sender.location(in: self.closetTable)
        let index = self.closetTable.indexPathForRow(at: point)
        let outfit = userOutfits[index!.row]
        let vc = self.storyboard?.instantiateViewController(identifier: "AddOwnOutitVC") as! AddOwnOutitVC
        vc.isForEdit = true
        vc.outfit = outfit
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func deleteCloset(id:String){
        NetworkManager().deleteOutfit(id: id) { (error, message) in
                   DispatchQueue.main.async {
                       Utility.removeLoading(vc: self)
                       if error == nil{
                        self.fetchOutdress(showLoader: false)
                        let alert = Utility.makeAlert(title: "", message: message!!)
                           self.present(alert, animated: true, completion: nil)
                       }else{
                           let alert = Utility.makeAlert(title: "", message: error!)
                           self.present(alert, animated: true, completion: nil)
                       }
                   }
               }
    }
}
