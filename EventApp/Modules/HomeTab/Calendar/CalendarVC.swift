//
//  CalendarVC.swift
//  EventApp
//
//  Created by Pankaj Sharma on 22/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit
import FSCalendar

class CalendarVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var eventTable: UITableView!
    @IBOutlet weak var eventTableHeight: NSLayoutConstraint!
    
    var dateFormatter:DateFormatter{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }
    
    var eventDates = [String]()
    
    var events = [UserEvents]()
    var date = Date()
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        calendar.delegate = self
        calendar.dataSource = self
        eventTable.register(UINib(nibName: "EventCell", bundle: nil), forCellReuseIdentifier: "EventCell")
        eventTable.delegate = self
        eventTable.dataSource = self
        
        let mdate = dateFormatter.string(from: date)
        fetchEvents(shouldLoad:true,date:mdate)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let mdate = dateFormatter.string(from: date)
        fetchEvents(shouldLoad:false,date:mdate)
    }

    func fetchEvents(shouldLoad:Bool,date:String){
        if shouldLoad{
            Utility.showLoading(vc: self)
        }
//        NetworkManager().getEvent(page: "1", limit: "20", parameter: ["date":date]) { (error, data) in
//            DispatchQueue.main.async {
//                Utility.removeLoading(vc: self)
//                if error == nil{
//                    self.events = data!
//
//                    for event in self.events{
//
//                        print(date)
//                    }
//
//                    self.calendar.reloadData()
//                    self.eventTableHeight.constant = CGFloat(self.events.count *  80)
//                    self.eventTable.reloadData()
//                }else{
//                    let alert = Utility.makeAlert(title: "", message: error!)
//                    self.present(alert, animated: true, completion: nil)
//                }
//            }
//        }
       
    NetworkManager().userEvents(page: "1", limit: "20", parameter: ["date":date]) { (error, data) in
               DispatchQueue.main.async {
                   Utility.removeLoading(vc: self)
                   if error == nil{
                       self.events = data!
                       for event in self.events{
                           let dateTime = event.start_datetime!.split(separator: " ")
                           let date = String(dateTime.first!)
                           self.eventDates.append(date)
                           print(date)
                       }
                        
                       self.calendar.reloadData()
                       self.eventTableHeight.constant = CGFloat(self.events.count *  80)
                       self.eventTable.reloadData()
                   }else{
                       let alert = Utility.makeAlert(title: "", message: error!)
                       self.present(alert, animated: true, completion: nil)
                   }
               }
           }

    }
    
    @IBAction func addEventTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "AddEventVC") as! AddEventVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func convertToDictionary(text: String) -> [[String: Any]]? {
               if let data = text.data(using: .utf8) {
                   do {
                       return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]
                   } catch {
                       print(error.localizedDescription)
                   }
               }
               return nil
           }
}

extension CalendarVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventCell
       let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell") as! EventCell
        cell.titleLabel.text = events[indexPath.row].title
        cell.subTitleLabel.text = events[indexPath.row].start_datetime
        cell.index = indexPath
        cell.selectedBlock = {[weak self](index) in
            let vc = self?.storyboard?.instantiateViewController(identifier: "BestMatchesVC") as! BestMatchesVC
            let mOutfits = self?.convertToDictionary(text: self?.events[index.row].outfits ?? "")
            vc.outfits = mOutfits as! [[String : [String : Any]]]
            self?.present(vc, animated: true, completion: nil)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension CalendarVC:FSCalendarDelegate,FSCalendarDelegateAppearance,FSCalendarDataSource{
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let mDate = dateFormatter.string(from: date)
        self.date = date
        fetchEvents(shouldLoad:true,date:mDate)
    }
    
       func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
         let dateString = self.dateFormatter.string(from: date)
         if self.eventDates.contains(dateString) {
             return 1
         }
         return 0
     }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        
         let key = self.dateFormatter.string(from: date)
         if self.eventDates.contains(key) {
             return [UIColor.white]
         }
         return nil
     }
}
