//
//  EventCell.swift
//  EventApp
//
//  Created by Pankaj Sharma on 22/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var calendarIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    var index:IndexPath?
    var selectedBlock:((IndexPath)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Utility.makeBorder(mView: backView, radius: 10, width: 0.3, color: .white)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func getDresstapped(_ sender: Any) {
        selectedBlock?(index!)
    }
}
