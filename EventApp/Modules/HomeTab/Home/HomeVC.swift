//
//  HomeVC.swift
//  EventApp
//
//  Created by Pankaj Sharma on 22/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage

class HomeVC: UIViewController {
    
    //MARK: - IBoutlets
    @IBOutlet weak var dressCV: UICollectionView!
    @IBOutlet weak var ratingVie: CosmosView!
    var currentIndex = 0
    var rating = 2.0
     let mArray = [["desc":"Denim shirt solid long sleeve","img":#imageLiteral(resourceName: "img.png")],["desc":"Federate Mens L/S Shirt","img":#imageLiteral(resourceName: "img2")]]
    var outfitData = [UserOutfits]()
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        dressCV.register(UINib(nibName: "OutfitCell", bundle: nil), forCellWithReuseIdentifier: "OutfitCell")
            dressCV.delegate = self
            dressCV.dataSource = self
        ratingVie.isHidden = true
        ratingVie.didFinishTouchingCosmos = {
            (rating) in
            self.ratingVie.text = "\(rating)"
            if (rating <= 2.0){
                self.rating = rating
                self.showAlert(rating:rating)
            }else{
                NetworkManager().rateOutfits(parameter: ["outfit_id":self.outfitData[self.currentIndex].id ?? "0","rate":"\(rating)","comments":"Good"]) { (error, message) in
                    DispatchQueue.main.async {
                        if error == nil{
                            
                        }else{
                            let alert = Utility.makeAlert(title: "", message: error!)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
        fetchOutdress(showLoader: true)
    }
    
//    func fetchOutdress(){
//        Utility.showLoading(vc: self)
//        NetworkManager().getOutfits(page: "1", limit: "20", parameter: ["weather":"sunny"]) { (error, outfitData) in
//            DispatchQueue.main.async {
//                Utility.removeLoading(vc: self)
//                if error == nil{
//                    self.outfitData = outfitData ?? []
//                    self.dressCV.reloadData()
//                    if outfitData!.count > 0{
//                        self.ratingVie.isHidden = false
//                    }
//                }else{
//                    let alert = Utility.makeAlert(title: "", message: error!)
//                    self.present(alert, animated: true, completion: nil)
//                }
//            }
//        }
//    }
    
    func fetchOutdress(showLoader:Bool){
        if showLoader{
            Utility.showLoading(vc: self)
        }
        
        NetworkManager().getUserOutfit(page: "1", limit: "20") { (error, muserOutfits) in
            DispatchQueue.main.async {
                Utility.removeLoading(vc: self)
                if error == nil{
                                   self.outfitData = muserOutfits ?? []
                                   self.dressCV.reloadData()
                                   if muserOutfits!.count > 0{
                                    self.view.bringSubviewToFront(self.dressCV)
                                       //self.ratingVie.isHidden = false
                                   }
                               }else{
                                   let alert = Utility.makeAlert(title: "", message: error!)
                                   self.present(alert, animated: true, completion: nil)
                               }
            }
        }
    }
    
    func showAlert(rating:Double){
        let alert = RatingAlert(frame: self.view.frame, delegate: self)
        self.view.addSubview(alert)
        self.view.bringSubviewToFront(alert)
        alert.headingLbl.text = "Please give the reason behind these \(rating) star rating!"
    }
}

extension HomeVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,RatingDelegate{
  
   
    func closeTapped() {
        
    }
    
    func submitTapped(comment:String) {
        Utility.showLoading(vc: self)
        NetworkManager().rateOutfits(parameter: ["outfit_id":self.outfitData[self.currentIndex].id ?? "0","rate":"\(rating)","comment":"Good"]) { (error, message) in
                       DispatchQueue.main.async {
                        Utility.removeLoading(vc: self)
                           if error == nil{
                               
                           }else{
                               let alert = Utility.makeAlert(title: "", message: message!)
                               self.present(alert, animated: true, completion: nil)
                           }
                       }
                   }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return outfitData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OutfitCell", for: indexPath) as! OutfitCell
        if let imgUrl = URL(string:"http://3.17.254.50/outdress/media/" + (outfitData[indexPath.row].picture ?? "")){
            cell.img.sd_setImage(with: imgUrl)
        }
       
        cell.check.isHidden = true
        cell.desc.text = outfitData[indexPath.row].name
        cell.cellWidth.constant = UIScreen.main.bounds.size.width - 48
        cell.cellHeight.constant = dressCV.frame.size.height
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = self.dressCV.frame.size.height
        let width = UIScreen.main.bounds.size.width - 48
        return CGSize(width: width, height: height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == dressCV{
            let index = scrollView.contentOffset.x/UIScreen.main.bounds.size.width
            currentIndex = Int(index)
        }
    }
}

