//
//  SettingsVC.swift
//  EventApp
//
//  Created by Pankaj Sharma on 22/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var settingTable: UITableView!
    
    let settingsArray = [["name":"Change Password","icon":#imageLiteral(resourceName: "password.png")],["name":"Change City / Weather","icon":#imageLiteral(resourceName: "location")],["name":"Contact Us","icon":#imageLiteral(resourceName: "contact")],["name":"Privacy Policy","icon":#imageLiteral(resourceName: "privacy")],["name":"Logout","icon":#imageLiteral(resourceName: "logout")]]
    
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        settingTable.register(UINib(nibName: "SettingCell", bundle: nil), forCellReuseIdentifier: "SettingCell")
        settingTable.delegate = self
        settingTable.dataSource = self
        settingTable.tableFooterView = UIView()
    }

}

extension SettingsVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
        cell.titleLabel.text = settingsArray[indexPath.row]["name"] as? String
        cell.iconView.image = settingsArray[indexPath.row]["icon"] as? UIImage
        if indexPath.row == 4{
            cell.arrow.isHidden = true
        }
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row{
        case 0:
            let vc = self.storyboard?.instantiateViewController(identifier: "ChangePasswordVC") as! ChangePasswordVC
            self.navigationController?.pushViewController(vc, animated: true)
        case 1:
            let vc = self.storyboard?.instantiateViewController(identifier: "ChangeLocationVC") as! ChangeLocationVC
                       self.navigationController?.pushViewController(vc, animated: true)
        case 4:
            let vc = self.storyboard?.instantiateViewController(identifier: "LoginVC") as! LoginVC
            let navVC = UINavigationController(rootViewController: vc)
            navVC.isNavigationBarHidden = true
            UserDefaults.standard.setLoggedIn(logged: false)
            let delegate = (UIApplication.shared.delegate as! AppDelegate)
            delegate.window?.rootViewController = navVC
            delegate.window?.makeKeyAndVisible()
        default:
            break
        }
    }
}
