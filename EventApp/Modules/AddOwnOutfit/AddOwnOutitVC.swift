//
//  AddOwnOutitVC.swift
//  EventApp
//
//  Created by Pankaj Sharma on 08/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit
import SDWebImage
import MobileCoreServices
import CoreML
import Vision
import DropDown


class AddOwnOutitVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var outfitImgV: UIImageView!
    @IBOutlet weak var titlrLbl: UILabel!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var colorTF: UITextField!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var uploadTF: UITextField!
    @IBOutlet weak var uploadView: UIView!    
    @IBOutlet weak var saveView: UIView!
    @IBOutlet weak var heightImage: NSLayoutConstraint!
    @IBOutlet weak var typeTF: UITextField!
    @IBOutlet weak var typeView: UIView!
    @IBOutlet weak var selectedColorView: UIView!
    
    //MARK: - Properties
    var isForEdit = false
    var outfit:UserOutfits!
    let picker = UIImagePickerController()
    var selectedImage:UIImage!
    var colorPickerView: ColorPickerView!
    var pickerTransform:CGAffineTransform!
    
    var allTypes = ["Shorts","Lowers","Suit","Jacket","Jacket","Formal Pants","Jeeans","Shirts with Button","T-Shirt"]
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.makeBorder(mView: nameView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: colorView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: uploadView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: saveView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: typeView, radius: 10, width: 1, color: .white)
        heightImage.constant = 0
        Utility.setPlaceholder(textField: nameTF)
        Utility.setPlaceholder(textField: colorTF)
        Utility.setPlaceholder(textField: uploadTF)
        Utility.setPlaceholder(textField: typeTF)
        colorPickerView = ColorPickerView(frame: self.view.bounds,delegate: self)
        pickerTransform = colorPickerView.viewColors.transform
        view.addSubview(colorPickerView)
        
        colorPickerView.viewColors.transform = pickerTransform.translatedBy(x: 0, y: 600)
        colorPickerView.isHidden = true
        if isForEdit{
            nameTF.text = outfit.name
            colorTF.text = outfit.color
            self.selectedColorView.backgroundColor = UIColor(hex: outfit.color ?? "")
            titlrLbl.text = "Edit your outfit"
            typeTF.text = outfit.type
            if let imgUrl = URL(string: ApiUrls.mediaUrl + (outfit.picture ?? "")){
                outfitImgV.sd_setImage(with: imgUrl)
                heightImage.constant = 200
            }
        }
//        self.view.isUserInteractionEnabled = true
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
//        self.view.addGestureRecognizer(tapGesture)
    }
    
//    @objc func viewTapped(_ sender:UITapGestureRecognizer){
//        hideColors()
//    }
//
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        if isForEdit{
            updateOutfit()
        }else{
            saveOutfit()
        }
    }
    
    func showColors(){
        colorPickerView.isHidden = false
        UIView.animate(withDuration: 0.2) {
            self.colorPickerView.viewColors.transform = self.pickerTransform
        }
    }
    
    func hideColors(){
        let downTransform = self.pickerTransform.translatedBy(x: 0, y: 600)
        UIView.animate(withDuration: 0.2, animations: {
            self.colorPickerView.viewColors.transform  = downTransform
        }) { (finished) in
            self.colorPickerView.isHidden = true
        }
        
    }
    
    @IBAction func btnActionColor(_ sender: Any) {
        showColors()
//        let colorArray = ["Black","White","Gray","Light Gray","Pink","Yellow","Red","Orange","Blue","Brown","Green"]
//        let dropDown = DropDown()
//        dropDown.anchorView = sender as! UIView
//        dropDown.dataSource = colorArray
//        dropDown.show()
//        dropDown.selectionAction = { [unowned self](index:Int,item:String)  in
//            self.colorTF.text = item
//            dropDown.hide()
//        }
    }
    
    @IBAction func typeSTapped(_ sender: Any) {
                let dropDown = DropDown()
                dropDown.anchorView = sender as! UIView
                dropDown.dataSource = allTypes
                dropDown.show()
                dropDown.selectionAction = { [unowned self](index:Int,item:String)  in
                    self.typeTF.text = item
                    dropDown.hide()
                }
    }
    
    
    
    @IBAction func uploadImageTapped(_ sender: Any) {
        presentImagePicker()
    }
    
    func updateOutfit(){
        let name = nameTF.text!
        let color = colorTF.text!
        let type = typeTF.text!
        if name.isEmpty || color.isEmpty || type.isEmpty{
            let alert = Utility.makeAlert(title: "", message:"Required fields can't be left empty!")
            self.present(alert, animated: true, completion: nil)
        }else{
            let params = ["name":name,"color":color,"type":type]
                  Utility.showLoading(vc: self)
            if selectedImage == nil{
                NetworkManager().editOutfitWithoutImage(id: outfit!.id!, parameter: params) { (error, message) in
                        DispatchQueue.main.async {
                            Utility.removeLoading(vc: self)
                            if error == nil{
                            let alert = Utility.makeAlert(title: "", message: message!!)
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            let alert = Utility.makeAlert(title: "", message: error!)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            }else{
                NetworkManager().editOutfit(id: outfit!.id!, parameter: params, image: selectedImage) { (error, message) in
                                  DispatchQueue.main.async {
                                      Utility.removeLoading(vc: self)
                                      
                                      if error == nil{
                                          let alert = Utility.makeAlert(title: "", message: message!!)
                                          self.present(alert, animated: true, completion: nil)
                                      }else{
                                          let alert = Utility.makeAlert(title: "", message: error!)
                                          self.present(alert, animated: true, completion: nil)
                                      }
                                  }
                        }
            }
        
        }
    }
    
    func saveOutfit(){
        let name = nameTF.text!
        let color = colorTF.text!
        let type = typeTF.text!
        if name.isEmpty || color.isEmpty || type.isEmpty{
            let alert = Utility.makeAlert(title: "", message:"Required fields can't be left empty!")
            self.present(alert, animated: true, completion: nil)
        }else{
            let params = ["name":name,"color":color,"type":type]
                
            if selectedImage == nil{
               let alert = Utility.makeAlert(title: "", message:"Please select an image to upload first!")
               self.present(alert, animated: true, completion: nil)
            }else{
                  Utility.showLoading(vc: self)
                NetworkManager().addOutfit(parameter: params, image: selectedImage) { (error, message) in
                        DispatchQueue.main.async {
                        Utility.removeLoading(vc: self)
                
                                if error == nil{
                                    let mAlert = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                    mAlert.addAction(okAction)
                                   
                                    self.present(mAlert, animated: true, completion: nil)
                                }else{
                            let alert = Utility.makeAlert(title: "", message: error!)
                            self.present(alert, animated: true, completion: nil)
                       }
                    }
                }
            }
        
        }
    }
    
}


extension AddOwnOutitVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate, ColorPickerDelegate{
    
    func didSelectColor(color: UIColor, hexString: String) {
        hideColors()
        self.colorTF.text = "#" + hexString
        self.selectedColorView.backgroundColor = color//UIColor(hex: "#\(hexString)")
    }
    
    func didSelectClose(){
        hideColors()
    }
    

//MARK: - Image Picker Delegate Methods
func presentImagePicker(){
  
    let alert = UIAlertController(title: "Photo Options", message: "", preferredStyle: .actionSheet)
    let cameraAction = UIAlertAction(title: "Take Photo", style: .default) { (action) in
        self.takePhoto()
    }
    let galleryAction = UIAlertAction(title: "Select From Gallery", style: .default) { (action) in
        self.selectPhoto()
    }
    let cancelAction = UIAlertAction(title: "Cancel", style: .destructive){
        (action) in
        self.dismiss(animated: true, completion: nil)
    }
    alert.addAction(cameraAction)
    alert.addAction(galleryAction)
    alert.addAction(cancelAction)
    self.present(alert, animated: true, completion: nil)
}

//MARK: - Capture Image from Camera Method
func takePhoto(){
    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
        picker.delegate = self
        self.picker.sourceType = UIImagePickerController.SourceType.camera
        picker.allowsEditing = false
        picker.showsCameraControls = true
        picker.cameraCaptureMode = .photo
        self .present(self.picker, animated: true, completion: nil)
    }
    else {
        let alert = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
        let OKBtn = UIAlertAction(title: "OK", style: .default) { handler  in
        }
        alert.addAction(OKBtn)
        present(alert, animated: true, completion: nil)
    }
}

//MARK: - Select Image from Gallery Option
func selectPhoto(){
    picker.allowsEditing = false
    picker.sourceType = .photoLibrary
    picker.delegate = self
    picker.modalPresentationStyle = .overCurrentContext
    picker.mediaTypes = [kUTTypeImage as String]
    present(picker, animated: true, completion: nil)
}

//MARK: - Image Picked Method
public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
    let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
    
    
    guard let ciImage = CIImage(image: image) else {
                       fatalError("Not able to convert UIImage to CIImage")
                   }
    selectedImage = image
    self.outfitImgV.image = image
    self.heightImage.constant = 200
    detectScene(image: ciImage)
    self.dismiss(animated: true, completion: nil)
        
    }
    
    // MARK: - Methods
      func detectScene(image: CIImage) {
        
          // Load the ML model through its generated class
          // Setup a classification request.
                guard let modelURL = Bundle.main.url(forResource: "ImageClassifier", withExtension: "mlmodelc") else {
                  print("Error")
                    return
                }
        
          let model = (try? VNCoreMLModel(for: MLModel(contentsOf: modelURL)))!
          
          // Create a Vision request with completion handler
          let request = VNCoreMLRequest(model: model) { [weak self] request, error in
              guard let results = request.results as? [VNClassificationObservation],
                  let topResult = results.first else {
                      fatalError("Unexpected result type from VNCoreMLRequest")
              }
              
              // Update UI on main queue
              DispatchQueue.main.async { [weak self] in
                if Int(topResult.confidence * 100) < 70{
                   
                    let alert = Utility.makeAlert(title: "Error", message: "Unable to get cloth type from this image. Please try again!")
                    self?.present(alert, animated: true, completion: nil)
                }else{
                    
                    self?.typeTF.text = topResult.identifier
                }
                
                //print("\(Int(topResult.confidence * 100))% it's \(topResult.identifier)")
              }
          }
          
          // Run the Core ML model classifier on global dispatch queue
          let handler = VNImageRequestHandler(ciImage: image)
          DispatchQueue.global(qos: .userInteractive).async {
              do {
                  try handler.perform([request])
              } catch {
                  print(error)
              }
          }
      }
    
}
