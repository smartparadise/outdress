//
//  AddEventVC.swift
//  EventApp
//
//  Created by Pankaj Sharma on 22/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit
import DropDown

class AddEventVC: UIViewController {
    
    @IBOutlet weak var timeTF: UITextField!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var saveView: UIView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var outfitTF: UITextField!
    @IBOutlet weak var dateTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var outfitView: UIView!
    @IBOutlet weak var endTimeVIew: UIView!
    @IBOutlet weak var typeView: UIView!
    @IBOutlet weak var typeTF: UITextField!
    @IBOutlet weak var endTimeTF: UITextField!
    
    
    
    var eventTypes = ["Office","Office Meeting","Party","Trekking","Tour"]
    
    var dateFormatter:DateFormatter{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }
    
    var timeFormatter:DateFormatter{
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        return formatter
    }
    
    var anotherFormatter:DateFormatter{
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm:ss"
        return formatter
    }
    
    var time = Date()
    var userOutfits = [UserOutfits]()
    var date = Date()
    var outfitIds = ""
    var mTime = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.makeBorder(mView: nameView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: timeView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: dateView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: saveView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: outfitView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: typeView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: endTimeVIew, radius: 10, width: 1, color: .white)
        
        Utility.setPlaceholder(textField: nameTF)
        Utility.setPlaceholder(textField: timeTF)
        Utility.setPlaceholder(textField: dateTF)
        Utility.setPlaceholder(textField: typeTF)
        Utility.setPlaceholder(textField: endTimeTF)
        Utility.setPlaceholder(textField: outfitTF)
        fetchOutdress(showLoader: false)
        
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .dateAndTime
        dateTF.inputView = datePicker
        datePicker.addTarget(self, action: #selector(onDatePickerValueChanged), for: .valueChanged)
        let datePicker2 = UIDatePicker()
        datePicker2.datePickerMode = .dateAndTime
        endTimeTF.inputView = datePicker
        datePicker2.addTarget(self, action: #selector(onDatePickerValueChanged2), for: .valueChanged)
    
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneDatePickerPressed))
        
        toolBar.setItems([space, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        let toolBar2 = UIToolbar()
               toolBar2.barStyle = UIBarStyle.default
               toolBar2.isTranslucent = true
               let space2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
               let doneButton2 = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneDatePickerPressed2))
               
               toolBar2.setItems([space2, doneButton2], animated: false)
               toolBar2.isUserInteractionEnabled = true
               toolBar2.sizeToFit()
        
        dateTF.inputAccessoryView = toolBar
        endTimeTF.inputAccessoryView = toolBar2
        
        let timeTool = UIToolbar()
        timeTool.barStyle = UIBarStyle.default
        timeTool.isTranslucent = true
        let space1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton1 = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneTimePickerPresed))
        
        timeTool.setItems([space1, doneButton1], animated: false)
        timeTool.isUserInteractionEnabled = true
        timeTool.sizeToFit()
        
        timeTF.inputAccessoryView = timeTool
        
    }
    
    @objc func doneDatePickerPressed(){
        self.dateTF.text = dateFormatter.string(from: date)
        self.view.endEditing(true)
    }
    
    @objc func doneDatePickerPressed2(){
        self.endTimeTF.text = dateFormatter.string(from: date)
        self.view.endEditing(true)
    }
    
    
    func fetchOutdress(showLoader:Bool){
        if showLoader{
            Utility.showLoading(vc: self)
        }
        
        NetworkManager().getUserOutfit(page: "1", limit: "20") { (error, userOutfits) in
            DispatchQueue.main.async {
                Utility.removeLoading(vc: self)
                if error == nil{
                    self.userOutfits = userOutfits ?? []
                    
                }else{
                    let alert = Utility.makeAlert(title: "", message: error!)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func doneTimePickerPresed(){
        self.timeTF.text = timeFormatter.string(from: time)
        mTime = anotherFormatter.string(from: time)
        self.view.endEditing(true)
    }
    
    @objc func onDatePickerValueChanged(datePicker: UIDatePicker) {
        date = datePicker.date
        
    }
    @objc func onDatePickerValueChanged2(datePicker: UIDatePicker) {
           date = datePicker.date
           
       }
    
    @objc func onTimePickerValueChanged(datePicker: UIDatePicker) {
        time = datePicker.date
        
    }
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveTappedx(_ sender: Any) {
        
        let name = nameTF.text!
        //let time = timeTF.text!
        let date = dateTF.text!
        let endDate = endTimeTF.text!
        let type = typeTF.text!
        
        
        if name.isEmpty  || date.isEmpty || endDate.isEmpty || type.isEmpty{
            let alert = Utility.makeAlert(title:"" , message: "Required Fields cannot be left empty!")
            self.present(alert, animated: true, completion: nil)
            }else{
            Utility.showLoading(vc: self)
            let outfits = getSuitableMatch()
            NetworkManager().addEvent(parameter: ["title":name,"start_datetime": date ,"end_datetime":endDate,"outfits":outfits,"category":type]) { (error, message) in
                DispatchQueue.main.async {
                    Utility.removeLoading(vc: self)
                    if error == nil{
                        let alert = UIAlertController(title: "Success", message: message!, preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                            self.navigationController?.popViewController(animated: true)
                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        let alert = Utility.makeAlert(title:"" , message: error!)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    
    @IBAction func selectOUtfitsTapped(_ sender: Any) {
//        let selectionView = SelectOutfits(frame: self.view.bounds, delegate: self)
//        selectionView.outfits = self.userOutfits
//        self.view.addSubview(selectionView)
        
        let dropDown = DropDown()
        dropDown.anchorView = sender as! UIButton
        dropDown.dataSource = eventTypes
        dropDown.show()
        dropDown.selectionAction = {
            [unowned self] (index:Int,item:String) in
            self.typeTF.text = item
        }
    }
    
}

extension AddEventVC:SelectionDelegate{
    func outfitSelected(ids: String, names: String) {
        self.outfitTF.text = names
        outfitIds = ids
    }
    
    
    func getSuitableMatch()->String{
        var allTypes = ["Shorts","Lowers","Suit","Jacket","Formal Pants","Jeans","Shirts with Button","T-Shirt"]
        let selectedType = self.typeTF.text!
        let allOutfits = Utility.shared.getOutfits()
        let shortsArray = allOutfits.filter({$0.type == "Shorts"})
        let lowerArray = allOutfits.filter({$0.type == "Lowers"})
        let suitArray = allOutfits.filter({$0.type == "Suit"})
        let jacketArray = allOutfits.filter({$0.type == "Jacket"})
        let formalPantArray = allOutfits.filter({$0.type == "Formal Pants"})
        let jeansArray = allOutfits.filter({$0.type == "Jeans"})
        let shirtsArray = allOutfits.filter({$0.type == "Shirts with Button"})
        let tshirtArray = allOutfits.filter({$0.type == "T-Shirt"})
       
        switch selectedType{
            case "Office":
                var bottom:UserOutfits!
                var top:UserOutfits!
                if formalPantArray.count > 0{
                    let random = Int.random(in: 0...formalPantArray.count-1)
                     bottom = formalPantArray[random]
                }
                
                if shirtsArray.count > 0{
                    let random = Int.random(in: 0...shirtsArray.count-1)
                     top = shirtsArray[random]
                }
                
                let selectedOutfit = [["upper":["id":top.id,"type":top.type,"name":top.name,"color":top.color,"picture":top.picture],"lower":["id":bottom.id,"type":bottom.type,"name":bottom.name,"color":bottom.color,"picture":bottom.picture]]]
                
                let outfit = selectedOutfit.toJson()
                print(outfit)
              
                
                
            return outfit
        case "Office Meeting":
            var bottom:UserOutfits!
                           var top:UserOutfits!
                           if formalPantArray.count > 0{
                               let random = Int.random(in: 0...formalPantArray.count-1)
                                bottom = formalPantArray[random]
                           }
                           
                           if suitArray.count > 0{
                               let random = Int.random(in: 0...suitArray.count-1)
                                top = suitArray[random]
                           }
            let selectedOutfit = [["upper":["id":top.id,"type":top.type,"name":top.name,"color":top.color,"picture":top.picture],"lower":["id":bottom.id,"type":bottom.type,"name":bottom.name,"color":bottom.color,"picture":bottom.picture]]]
            
            let outfit = selectedOutfit.toJson()
            print(outfit)
            
            return outfit
        case "Party":
            var bottom:UserOutfits!
            var top:UserOutfits!
            if jeansArray.count > 0{
                let random = Int.random(in: 0...jeansArray.count-1)
                 bottom = jeansArray[random]
            }
            
            if tshirtArray.count > 0{
                let random = Int.random(in: 0...tshirtArray.count-1)
                 top = tshirtArray[random]
            }
            let selectedOutfit = [["upper":["id":top.id,"type":top.type,"name":top.name,"color":top.color,"picture":top.picture],"lower":["id":bottom.id,"type":bottom.type,"name":bottom.name,"color":bottom.color,"picture":bottom.picture]]]
            
            let outfit = selectedOutfit.toJson()
            print(outfit)
            return outfit
        case "Trekking":
            var bottom:UserOutfits!
                      var top:UserOutfits!
                      if shortsArray.count > 0{
                          let random = Int.random(in: 0...shortsArray.count-1)
                           bottom = shortsArray[random]
                      }
                      
                      if tshirtArray.count > 0{
                          let random = Int.random(in: 0...tshirtArray.count-1)
                           top = tshirtArray[random]
                      }
            let selectedOutfit = [["upper":["id":top.id,"type":top.type,"name":top.name,"color":top.color,"picture":top.picture],"lower":["id":bottom.id,"type":bottom.type,"name":bottom.name,"color":bottom.color,"picture":bottom.picture]]]
            
            let outfit = selectedOutfit.toJson()
            return outfit
            break
        case "Tour":
            var bottom:UserOutfits!
            var top:UserOutfits!
            if lowerArray.count > 0{
                let random = Int.random(in: 0...lowerArray.count-1)
                 bottom = lowerArray[random]
            }
            
            if tshirtArray.count > 0{
                let random = Int.random(in: 0...tshirtArray.count-1)
                 top = tshirtArray[random]
            }
            let selectedOutfit = [["upper":["id":top.id,"type":top.type,"name":top.name,"color":top.color,"picture":top.picture],"lower":["id":bottom.id,"type":bottom.type,"name":bottom.name,"color":bottom.color,"picture":bottom.picture]]]
            
            let outfit = selectedOutfit.toJson()
            print(outfit)
           return outfit
        default:
            return ""
            
        }
    }
}
