//
//  SelectOutfits.swift
//  EventApp
//
//  Created by Pankaj Sharma on 30/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import Foundation
import UIKit
protocol  SelectionDelegate {
    func outfitSelected(ids:String,names:String)
}

class SelectOutfits: UIView {
   
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var tbl: UITableView!
    @IBOutlet weak var doneBtn: UIButton!
    
    var outfits = [UserOutfits]()
    var selectedId = [String]()
    var selectedNames = [String]()
    var delegate:SelectionDelegate!
    
    init(frame: CGRect,delegate:SelectionDelegate) {
        super.init(frame:frame)
        self.delegate = delegate
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        var names = ""
        var ids = ""
        for name in selectedNames{
            names = name + "," + names
        }
        for id in selectedId{
            ids = id + "," + ids
        }
        let idss = ids.dropLast()
        let namess = names.dropLast()
        print(ids)
        delegate.outfitSelected(ids: String(idss), names: String(namess))
        self.removeFromSuperview()
    }
    
    
    func commonInit(){
        Bundle.main.loadNibNamed("SelectOutfits", owner: self, options: nil)
        self.contentView.frame = self.frame
        self.contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.addSubview(self.contentView)
      
        tbl.delegate = self
        tbl.dataSource = self
    }
    @IBAction func cancelTapped(_ sender: Any) {
         self.removeFromSuperview()
    }
    
   
}

extension SelectOutfits:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return outfits.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.textLabel?.textColor = .white
        cell.textLabel?.text = outfits[indexPath.row].name
        cell.selectionStyle = .none
        
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
        if selectedId.contains(outfits[indexPath.row].id!){
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedId.contains(outfits[indexPath.row].id!){
            let mAr = selectedId as NSArray
            let index = mAr.index(of: outfits[indexPath.row].id!)
            selectedId.remove(at: index)
            selectedNames.remove(at: index)
        }else{
            selectedNames.append(outfits[indexPath.row].name!)
            selectedId.append(outfits[indexPath.row].id!)
        }
        tbl.reloadData()
    }
}
