//
//  SelectGenderVC.swift
//  EventApp
//
//  Created by Pankaj Sharma on 22/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit

class SelectGenderVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var femaleCheck: UIImageView!
    @IBOutlet weak var maleCheck: UIImageView!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var nextView: UIView!
    
    var gender = "Male"
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        maleCheck.isHidden = false
        femaleCheck.isHidden = true
        Utility.makeBorder(mView: nextView, radius: 10, width: 1, color: .white)
    }

    //MARK: - IBAction Methods
    @IBAction func femaleTapped(_ sender: Any) {
        maleCheck.isHidden = true
        femaleCheck.isHidden = false
        gender = "Female"
    }
    
    @IBAction func maleTapped(_ sender: Any) {
        maleCheck.isHidden = false
        femaleCheck.isHidden = true
        gender = "Male"
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        Utility.showLoading(vc: self)
        NetworkManager().changeGender(parameter: ["gender":gender]) { (error, message) in
            DispatchQueue.main.async {
                Utility.removeLoading(vc: self)
                if error == nil{
                    let vc = self.storyboard?.instantiateViewController(identifier: "HomeTabBar") as! HomeTabBar
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    let alert = Utility.makeAlert(title: "", message: error!)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        
        
    }
}
