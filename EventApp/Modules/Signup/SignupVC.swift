//
//  SignupVC.swift
//  EventApp
//
//  Created by Pankaj Sharma on 08/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit

class SignupVC: UIViewController {
    
    @IBOutlet weak var checkIcon: UIImageView!
    @IBOutlet weak var firstView: UIView!
    
    @IBOutlet weak var firstTF: UITextField!
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passView: UIView!
    
    @IBOutlet weak var confirmView: UIView!
    
    @IBOutlet weak var passTF: UITextField!
    
    @IBOutlet weak var confirmTF: UITextField!
    
    @IBOutlet weak var signupView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       Utility.makeBorder(mView: emailView, radius: 10, width: 1, color: .white)
       Utility.makeBorder(mView: passView, radius: 10, width: 1, color: .white)
       Utility.makeBorder(mView: firstView, radius: 10, width: 1, color: .white)
       Utility.makeBorder(mView: confirmView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: signupView, radius: 10, width: 1, color: .white)
       
       Utility.setPlaceholder(textField: passTF)
       Utility.setPlaceholder(textField: emailTF)
        Utility.setPlaceholder(textField: confirmTF)
        Utility.setPlaceholder(textField: firstTF)
    }
    
    @IBAction func signInTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signupTapped(_ sender: Any) {
        let email = emailTF.text!
        let password = passTF.text!
        let name = firstTF.text!
        let confirm = confirmTF.text!
              
        if email.isEmpty || password.isEmpty || name.isEmpty || confirm.isEmpty{
                  let alert = Utility.makeAlert(title:"" , message: "Required Fields cannot be left empty!")
                  self.present(alert, animated: true, completion: nil)
              }else if !Utility.isValidEmail(testStr: email){
                  let alert = Utility.makeAlert(title:"" , message: "Please enter valid email address!")
                  self.present(alert, animated: true, completion: nil)
        }else if password != confirm {
            let alert = Utility.makeAlert(title:"" , message: "Password didn't match!")
                             self.present(alert, animated: true, completion: nil)
        }else{
                  Utility.showLoading(vc: self)
            NetworkManager().signup(parameter: ["first_name":name,"last_name":" ","email":email,"password":password,"device_type":"IOS","device_token":"1234"]) { (error, message) in
                      DispatchQueue.main.async {
                          Utility.removeLoading(vc: self)
                          if error == nil{
                            let alert = UIAlertController(title: "Success", message: message!, preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                self.navigationController?.popViewController(animated: true)
                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                          }else{
                              let alert = Utility.makeAlert(title:"" , message: error!)
                              self.present(alert, animated: true, completion: nil)
                          }
                      }
                  }
              }
              
    }
    
    @IBAction func picTapped(_ sender: Any) {
    }
    
    @IBAction func termsTapped(_ sender: Any) {
    }
}
