//
//  ChangePasswordVC.swift
//  EventApp
//
//  Created by Pankaj Sharma on 22/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    @IBOutlet weak var submitView: UIView!
    @IBOutlet weak var confirmTF: UITextField!
    @IBOutlet weak var confirmView: UIView!
    @IBOutlet weak var newTF: UITextField!
    @IBOutlet weak var newView: UIView!
    @IBOutlet weak var oldTF: UITextField!
    @IBOutlet weak var oldView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.makeBorder(mView: submitView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: confirmView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: newView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: oldView, radius: 10, width: 1, color: .white)
               
            
        Utility.setPlaceholder(textField: oldTF)
        Utility.setPlaceholder(textField: newTF)
                          Utility.setPlaceholder(textField: confirmTF)
              
    }
    

    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        let pass = oldTF.text!
        let new = newTF.text!
        let confirm = confirmTF.text!
        if pass.isEmpty || new.isEmpty || confirm.isEmpty{
                let alert = Utility.makeAlert(title:"" , message: "Required Fields cannot be left empty!")
                self.present(alert, animated: true, completion: nil)
            }else if new != confirm{
                let alert = Utility.makeAlert(title:"" , message: "Password didn't match!")
                self.present(alert, animated: true, completion: nil)
            }else{
                Utility.showLoading(vc: self)
            NetworkManager().changePassword(parameter: ["oldpassword":pass,"newpassword":new]) { (error, message) in
                    DispatchQueue.main.async {
                        Utility.removeLoading(vc: self)
                        if error == nil{
                            let alert = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                self.navigationController?.popViewController(animated: true)
                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            let alert = Utility.makeAlert(title: "", message: error!)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                    
                }
            }
    }
}
