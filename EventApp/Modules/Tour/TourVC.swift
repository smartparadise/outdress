//
//  TourVC.swift
//  EventApp
//
//  Created by Pankaj Sharma on 08/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit

class TourVC: UIViewController {

    @IBOutlet weak var secondC: UIImageView!
    @IBOutlet weak var firstC: UIImageView!
    @IBOutlet weak var thirdC: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
   var currentIndex = 0
    
    @IBOutlet weak var imgCV: UICollectionView!
    
    let imgArray = [UIImage(named: "firstT"),UIImage(named: "secondT"),UIImage(named: "thirdT")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Utility.makeViewRound(mView: firstC)
        Utility.makeViewRound(mView: secondC)
        Utility.makeViewRound(mView: thirdC)
        imgCV.register(UINib(nibName: "TourCell", bundle: nil), forCellWithReuseIdentifier: "TourCell")
        imgCV.delegate = self
        imgCV.dataSource = self
        makeCircle(index: currentIndex)
    }
    
    func makeCircle(index:Int){
        switch index{
        case 0:
            firstC.backgroundColor = .white
            secondC.backgroundColor = .clear
            thirdC.backgroundColor = .clear
            case 1:
            firstC.backgroundColor = .clear
            secondC.backgroundColor = .white
            thirdC.backgroundColor = .clear
            case 2:
            firstC.backgroundColor = .clear
            secondC.backgroundColor = .clear
            thirdC.backgroundColor = .white
        default:
            print("")
        }
    }
    

    @IBAction func skipTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "LoginVC") as! LoginVC
                   self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        if currentIndex < 2{
                   imgCV.scrollToNextItem()
               }else{
            let vc = self.storyboard?.instantiateViewController(identifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(vc, animated: true)
               }
    }
}


extension TourVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TourCell", for:indexPath) as! TourCell
        cell.image.image = imgArray[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = UIScreen.main.bounds.size.height
        let width = UIScreen.main.bounds.size.width
        return CGSize(width: width, height:height + 100)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == imgCV{
            let index = Int(scrollView.contentOffset.x/UIScreen.main.bounds.size.width)
            currentIndex = index
            makeCircle(index: currentIndex)
        }
    }
    
}
