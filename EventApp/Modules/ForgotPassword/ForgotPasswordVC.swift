//
//  ForgotPasswordVC.swift
//  EventApp
//
//  Created by Pankaj Sharma on 08/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var sendView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.makeBorder(mView: emailView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: sendView, radius: 10, width: 1, color: .white)
        Utility.setPlaceholder(textField: emailTF)
        
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendTapped(_ sender: Any) {
        let email = emailTF.text!
        if email.isEmpty{
            let alert = Utility.makeAlert(title:"" , message: "Required Fields cannot be left empty!")
            self.present(alert, animated: true, completion: nil)
        }else if !Utility.isValidEmail(testStr: email){
            let alert = Utility.makeAlert(title:"" , message: "Please enter valid email address!")
            self.present(alert, animated: true, completion: nil)
        }else{
            Utility.showLoading(vc: self)
            NetworkManager().forgotPassword(parameter: ["email":email]) { (error, message) in
                DispatchQueue.main.async {
                    Utility.removeLoading(vc: self)
                    if error == nil{
                        let alert = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                            self.navigationController?.popViewController(animated: true)
                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        let alert = Utility.makeAlert(title: "", message: error!)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
                
            }
        }
        
    }
    
}
