//
//  AddLocationVC.swift
//  EventApp
//
//  Created by Pankaj Sharma on 08/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces

class AddLocationVC: UIViewController,CLLocationManagerDelegate {

    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var locView: UIView!
    @IBOutlet weak var locTF: UITextField!
   
   // var locationManager:CLLocationManager!
    var lat = 0.0
    var long = 0.0
    var address = "abc"
    var city = "abc"
    var state = "abc"
    var pincode = "abc"
    var country = "abc"
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.makeBorder(mView: nextView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: locView, radius: 10, width: 1, color: .white)
        Utility.setPlaceholder(textField: locTF)
        locTF.delegate = self
       
//        if (CLLocationManager.locationServicesEnabled())
//          {
//              locationManager = CLLocationManager()
//              locationManager.delegate = self
//              locationManager.desiredAccuracy = kCLLocationAccuracyBest
//              locationManager.requestAlwaysAuthorization()
//              locationManager.startUpdatingLocation()
//          }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let latitude = location?.coordinate.latitude ?? 0.0
        let longitude = location?.coordinate.longitude ?? 0.0
        lat = latitude
        long = longitude
        DispatchQueue.main.async {
            Utility.showLoading(vc: self)
        }
       // getAddressFromLatLon(pdblLatitude: latitude, withLongitude: longitude)
    }

    @IBAction func micTapped(_ sender: Any) {
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        
        Utility.showLoading(vc: self)
     
        NetworkManager().updateProfile(parameter: ["latitude":"\(lat)","longitude":"\(long)","country":country,"address":address,"city":city,"state":state,"pincode":pincode]){
            (error,message) in
            DispatchQueue.main.async {
                if error == nil{
                    let vc = self.storyboard?.instantiateViewController(identifier: "SelectGenderVC") as! SelectGenderVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    let alert = Utility.makeAlert(title: "", message: error!)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
//    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double)  {
//        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
//        let lat: Double = Double("\(pdblLatitude)")!
//
//        let lon: Double = Double("\(pdblLongitude)")!
//
//        let ceo: CLGeocoder = CLGeocoder()
//        center.latitude = lat
//        center.longitude = lon
//
//        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
//
//        ceo.reverseGeocodeLocation(loc, completionHandler:
//            {(placemarks, error) in
//                DispatchQueue.main.async {
//                    Utility.removeLoading(vc: self)
//                    if (error != nil)
//                    {
//                        print("reverse geodcode fail: \(error!.localizedDescription)")
//                    }
//                    let pm = placemarks! as [CLPlacemark]
//
//                    if pm.count > 0 {
//                        let pm = placemarks![0]
//
//                        var addressString : String = ""
//                        if pm.subLocality != nil {
//                            addressString = addressString + pm.subLocality! + ", "
//                            // self.cityTF.text = pm.
//                        }
//                        if pm.thoroughfare != nil {
//                            //  addressString = addressString + pm.thoroughfare! + ", "
//                        }
//
//                        if pm.locality != nil {
//
//                            addressString = addressString + pm.locality! + " "
//                            self.city = pm.locality!
//                        }
//                        if pm.administrativeArea != nil{
//                            addressString = addressString + pm.administrativeArea! + " "
//                            self.state = pm.administrativeArea!
//                        }
//                        if pm.country != nil {
//
//                            addressString = addressString + pm.country! + " "
//                            self.country = pm.country!
//                        }
//                        if pm.postalCode != nil {
//                            addressString = addressString + pm.postalCode! + " "
//                            self.pincode = pm.postalCode!
//
//                        }
//                        DispatchQueue.main.async {
//                             self.locTF.text = addressString
//                            self.address = addressString
//                            Utility.removeLoading(vc: self)
//                        }
//
//
//                    }
//                }
//
//        })
//        locationManager.stopUpdatingLocation()
//
//    }
}

extension AddLocationVC:UITextFieldDelegate,GMSAutocompleteViewControllerDelegate{
   
    //MARK: - GMSAutoComplete Delegate
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let c = place.coordinate
        self.lat = c.latitude
        self.long = c.longitude
        self.address = place.formattedAddress ?? ""
        print(place.addressComponents)
        self.city  = place.addressComponents?.first(where: { $0.type == "locality" })?.name ?? ""
        self.state = place.addressComponents?.first(where: { $0.type == "administrative_area_level_1" })?.name ?? ""
        self.country = place.addressComponents?.first(where: { $0.type == "country" })?.name ?? ""
        self.pincode = "0"
        locTF.text = self.address
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == locTF{
            let viewController = GMSAutocompleteViewController()
            viewController.delegate = self
            self.present(viewController, animated: true, completion: nil)
        }
    }
}
