//
//  MatchesCell.swift
//  EventApp
//
//  Created by Pankaj Sharma on 18/02/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class MatchesCell: UICollectionViewCell {
    
    @IBOutlet weak var imgUpper: UIImageView!
    
    @IBOutlet weak var imgLower: UIImageView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewWidth: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewWidth.constant = UIScreen.main.bounds.size.width * 0.7
        viewHeight.constant = UIScreen.main.bounds.size.height * 0.35
    }

}
