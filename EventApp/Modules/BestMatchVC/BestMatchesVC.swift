//
//  BestMatchesVC.swift
//  EventApp
//
//  Created by Pankaj Sharma on 18/02/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class BestMatchesVC: UIViewController {
    
    
    @IBOutlet weak var matchesCV: UICollectionView!
    
    var outfits = [[String:[String:Any]]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(outfits)
        matchesCV.register(UINib(nibName: "MatchesCell", bundle: nil), forCellWithReuseIdentifier: "MatchesCell")
        matchesCV.delegate = self
        matchesCV.dataSource = self
        
    }

    @IBAction func btnActionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension BestMatchesVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return outfits.count
}

func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MatchesCell", for: indexPath) as! MatchesCell
    let upperImg = outfits[indexPath.row]["upper"]?["picture"] as! String
    let lowerImg = outfits[indexPath.row]["lower"]?["picture"] as! String
    cell.imgUpper.sd_setImage(with:URL(string: ApiUrls.mediaUrl + upperImg))
    cell.imgLower.sd_setImage(with: URL(string: ApiUrls.mediaUrl + lowerImg))
    return cell
}
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.matchesCV.frame.size.width, height: self.matchesCV.frame.size.height)
    }

}
