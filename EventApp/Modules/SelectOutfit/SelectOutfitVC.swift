//
//  SelectOutfitVC.swift
//  EventApp
//
//  Created by Pankaj Sharma on 08/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit

class SelectOutfitVC: UIViewController {

    @IBOutlet weak var outfitCV: UICollectionView!
    @IBOutlet weak var nextView: UIView!
    
    let mArray = [["desc":"Denim shirt solid long sleeve","img":#imageLiteral(resourceName: "img.png")],["desc":"Federate Mens L/S Shirt","img":#imageLiteral(resourceName: "img2")]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        outfitCV.register(UINib(nibName: "OutfitCell", bundle: nil), forCellWithReuseIdentifier: "OutfitCell")
        outfitCV.delegate = self
        outfitCV.dataSource = self
        
        Utility.makeBorder(mView: nextView, radius: 10, width: 1, color: .white)
    }
    

    @IBAction func nextTapped(_ sender: Any) {
    }
    
    @IBAction func addOwnTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "AddOwnOutitVC") as! AddOwnOutitVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension SelectOutfitVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        mArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OutfitCell", for: indexPath) as! OutfitCell
        cell.img.image = mArray[indexPath.row]["img"] as! UIImage
        cell.desc.text = mArray[indexPath.row]["desc"] as! String
        cell.cellWidth.constant = UIScreen.main.bounds.size.width - 150
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = self.outfitCV.frame.size.height
        let width = UIScreen.main.bounds.size.width/2 - 50
        return CGSize(width: width, height: height)
    }
}
