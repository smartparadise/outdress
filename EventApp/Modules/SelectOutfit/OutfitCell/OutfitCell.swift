//
//  OutfitCell.swift
//  EventApp
//
//  Created by Pankaj Sharma on 08/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit

class OutfitCell: UICollectionViewCell {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var cellHeight: NSLayoutConstraint!
    
    @IBOutlet weak var cellWidth: NSLayoutConstraint!
    @IBOutlet weak var check: UIImageView!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var img: UIImageView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        Utility.makeCornerRounds(mView: backView, radius: 10)
        
    }

}
