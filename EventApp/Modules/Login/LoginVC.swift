//
//  LoginVC.swift
//  EventApp
//
//  Created by Pankaj Sharma on 08/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var checkIcon: UIImageView!
    @IBOutlet weak var passTF: UITextField!
    @IBOutlet weak var passView: UIView!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var signinView: UIView!
    
    var isRememberMe = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.makeBorder(mView: emailView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: passView, radius: 10, width: 1, color: .white)
        Utility.makeBorder(mView: signinView, radius: 10, width: 1, color: .white)
        
        Utility.setPlaceholder(textField: passTF)
        Utility.setPlaceholder(textField: emailTF)
        UserDefaults.standard.setFirstTime(isFirst: false)
        
    }
    
    
    @IBAction func signupTapped(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "SignupVC") as! SignupVC
               self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func rememberMeTappedx(_ sender: Any) {
        isRememberMe = !isRememberMe
        if isRememberMe{
            checkIcon.image = #imageLiteral(resourceName: "check1.png")
        }else{
            checkIcon.image = #imageLiteral(resourceName: "checkbox")
        }
    }
    
    @IBAction func forgotTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ForgotPasswordVC") as! ForgotPasswordVC
                     self.navigationController?.pushViewController(vc, animated: true)
       
    }
    
    @IBAction func signinTapped(_ sender: Any) {
        let email = emailTF.text!
        let password = passTF.text!
        
        if email.isEmpty || password.isEmpty{
            let alert = Utility.makeAlert(title:"" , message: "Required Fields cannot be left empty!")
            self.present(alert, animated: true, completion: nil)
        }else if !Utility.isValidEmail(testStr: email){
            let alert = Utility.makeAlert(title:"" , message: "Please enter valid email address!")
            self.present(alert, animated: true, completion: nil)
        }else{
            Utility.showLoading(vc: self)
            NetworkManager().login(parameter: ["email":email,"password":password,"device_type":"IOS","device_token":"1234","timezone":Calendar.current.timeZone.abbreviation()!]) { (error, data) in
                DispatchQueue.main.async {
                    Utility.removeLoading(vc: self)
                    if error == nil{
                        if data?.address == nil{
                            let vc = self.storyboard?.instantiateViewController(identifier: "AddLocationVC") as! AddLocationVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else{
                            let vc = self.storyboard?.instantiateViewController(identifier: "HomeTabBar") as! HomeTabBar
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }else{
                        let alert = Utility.makeAlert(title:"" , message: error!)
                                   self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        
    }
    
}
