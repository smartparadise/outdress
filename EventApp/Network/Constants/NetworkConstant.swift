//
//  AppDelegate.swift
//  EventApp
//
//  Created by Pankaj Sharma on 07/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//
import Foundation

//URL's used in app
struct ApiUrls{
    
    static let baseUrl = "http://3.17.254.50/outdress/api"
    static let privacy = "http://3.17.254.50/outdress/privacy.html"
    
    static let mediaUrl = "http://3.17.254.50/outdress/media/"
    
    static let signUp = "/signup"
    static let login = "/login"
    static let forgotPassword = "/forgot_password"
   
    static let changePassword = "/change_password"
    static let changeGender = "/update_gender"
    static let updateProfile = "/update_profile"
    static let getProfile = "/get_profile"
    static let signOut = "/sign_out"
    static let updateProfilePicture = "/update_profile_picture"
    static let getOutfits = "/get_outfits"
    static let rateOutfit = "/rate_outfit"
    
    static let addOutfit = "/add_outfit"
    static let getUserOutfit = "/user_outfits"
    static let deleteUserOutfit = "/delete_outfit" // /id
    static let editOutfit = "/edit_outfit" // /id
    
    static let deleteEvent = "/delete_event" // /id
    static let addEvent = "/add_event"
    static let getEvents = "/get_events" //date
    static let editEvent = "/edit_event" // /id
    static let userEvents = "/get_events"
 
}

class AuthHeader{
    
    static func makeAuth()->[String:String]{
         let username = "outdress"
         let password = "outdress123!@#"
         let loginString = String(format: "%@:%@", username, password)
         let loginData = loginString.data(using: String.Encoding.utf8)!
         let base64LoginString = loginData.base64EncodedString()
        
         let auth = ["Authorization":"Basic \(base64LoginString)"]
         return auth
     }
}

struct UserHeaders{
    
 static func makeAuth()->[String:String]{
         let username = "outdress"
         let password = "outdress123!@#"
         let loginString = String(format: "%@:%@", username, password)
         let loginData = loginString.data(using: String.Encoding.utf8)!
         let base64LoginString = loginData.base64EncodedString()
         let userId = UserDefaults.standard.getUserId()
    let sessionId = UserDefaults.standard.getSessionId()
    let auth = ["Authorization":"Basic \(base64LoginString)","userid":userId,"sessionid":sessionId]
         return auth
     }
    
}
