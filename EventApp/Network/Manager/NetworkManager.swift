//
//  AppDelegate.swift
//  EventApp
//
//  Created by Pankaj Sharma on 07/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//
import Foundation
import UIKit
import ObjectMapper

public struct NetworkManager{
    
 
    let startRouter = NetRouter<StartApis>()
    let generalRouter = NetRouter<GeneralApis>()
    
    //MARK: - Method to call login Api
    
    func login(parameter:[String:String],completion:@escaping(_ error:String?,_ data:UserData?)->Void){
        startRouter.request(.login(params: parameter)) { (data, response, error) in
            if error != nil{
                completion("Please check your internet connection!",nil)
            }
            if let response = response as? HTTPURLResponse{
                let result = self.handleNetworkResponse(response)
                switch result{
                case .success:
                    guard let responseData = data else{
                        completion(NetworkResponse.noData.rawValue,nil)
                        return
                    }
                    do{
                        let apiResponse = try JSONSerialization.jsonObject(
                            with: responseData, options: [])
                        
                        let login = Mapper<Login>().map(JSON: apiResponse as! [String : Any])
                        
                        UserDefaults.standard.setEmail(userId: login?.data?.email)
                        UserDefaults.standard.setUserId(userId: login?.data?.id)
                        UserDefaults.standard.setSessionId(sessionId: login?.data?.sessionId)
                        UserDefaults.standard.setLoggedIn(logged: true)
                        UserDefaults.standard.setAddress(address:login?.data?.address ?? "")
                     
                        UserDefaults.standard.synchronize()
                        completion(nil,login?.data)
                    }catch (let missingData){
                        print(missingData)
                        completion(NetworkResponse.unableToDecode.rawValue,nil)
                    }
                case .conditionFalse:
                    guard let responseData = data else{
                        completion(NetworkResponse.noData.rawValue,nil)
                        return
                    }
                    do{
                        let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                        print(json)
                        let message = (json as! NSDictionary).value(forKey: "message") as! String
                        completion(message,nil)
                    }catch{
                        completion(NetworkResponse.unableToDecode.rawValue,nil)
                    }

                case .failure(let networkFailure):
                    print(networkFailure)
                    completion(networkFailure,nil)
                }
            }
        }
    }

    func signup(parameter:[String:String],completion:@escaping(_ error:String?,_ message:String?)->Void){
        startRouter.request(.signup(params: parameter)) { (data, response, error) in
            if error != nil{
                completion("Please check your internet connection!",nil)
            }
            if let response = response as? HTTPURLResponse{
                let result = self.handleNetworkResponse(response)
                switch result{
                case .success:
                    guard let responseData = data else{
                        completion(NetworkResponse.noData.rawValue,nil)
                        return
                    }
                    do{
                        let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                        print(json)
                        let message = (json as! NSDictionary).value(forKey: "message") as! String
                        
                        completion(nil,message)
                    }catch (let missingData){
                        print(missingData)
                        completion(NetworkResponse.unableToDecode.rawValue,nil)
                    }
                case .conditionFalse:
                    guard let responseData = data else{
                        completion(NetworkResponse.noData.rawValue,nil)
                        return
                    }
                    do{
                        let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                        print(json)
                        let message = (json as! NSDictionary).value(forKey: "message") as! String
                        completion(message,nil)
                    }catch{
                        completion(NetworkResponse.unableToDecode.rawValue,nil)
                    }

                case .failure(let networkFailure):
                    print(networkFailure)
                    completion(networkFailure,nil)
                }
            }
        }
    }

    
    func forgotPassword(parameter:[String:String],completion:@escaping(_ error:String?,_ message:String?)->Void){
        startRouter.request(.forgot(params: parameter)) { (data, response, error) in
            if error != nil{
                completion("Please check your internet connection!",nil)
            }
            if let response = response as? HTTPURLResponse{
                let result = self.handleNetworkResponse(response)
                switch result{
                case .success:
                    guard let responseData = data else{
                        completion(NetworkResponse.noData.rawValue,nil)
                        return
                    }
                    do{
                        let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                        print(json)
                        let message = (json as! NSDictionary).value(forKey: "message") as! String
                        
                        completion(nil,message)
                    }catch (let missingData){
                        print(missingData)
                        completion(NetworkResponse.unableToDecode.rawValue,nil)
                    }
                case .conditionFalse:
                    guard let responseData = data else{
                        completion(NetworkResponse.noData.rawValue,nil)
                        return
                    }
                    do{
                        let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                        print(json)
                        let message = (json as! NSDictionary).value(forKey: "message") as! String
                        completion(message,nil)
                    }catch{
                        completion(NetworkResponse.unableToDecode.rawValue,nil)
                    }

                case .failure(let networkFailure):
                    print(networkFailure)
                    completion(networkFailure,nil)
                }
            }
        }
    }
    
    func changePassword(parameter:[String:String],completion:@escaping(_ error:String?,_ message:String?)->Void){
        generalRouter.request(.changePassword(params: parameter)) { (data, response, error) in
            if error != nil{
                completion("Please check your internet connection!",nil)
            }
            if let response = response as? HTTPURLResponse{
                let result = self.handleNetworkResponse(response)
                switch result{
                case .success:
                    guard let responseData = data else{
                        completion(NetworkResponse.noData.rawValue,nil)
                        return
                    }
                    do{
                        let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                        print(json)
                        let message = (json as! NSDictionary).value(forKey: "message") as! String
                        
                        completion(nil,message)
                    }catch (let missingData){
                        print(missingData)
                        completion(NetworkResponse.unableToDecode.rawValue,nil)
                    }
                case .conditionFalse:
                    guard let responseData = data else{
                        completion(NetworkResponse.noData.rawValue,nil)
                        return
                    }
                    do{
                        let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                        print(json)
                        let message = (json as! NSDictionary).value(forKey: "message") as! String
                        completion(message,nil)
                    }catch{
                        completion(NetworkResponse.unableToDecode.rawValue,nil)
                    }

                case .failure(let networkFailure):
                    print(networkFailure)
                    completion(networkFailure,nil)
                }
            }
        }
    }

    
    func changeGender(parameter:[String:String],completion:@escaping(_ error:String?,_ message:String?)->Void){
           generalRouter.request(.changeGender(params: parameter)) { (data, response, error) in
               if error != nil{
                   completion("Please check your internet connection!",nil)
               }
               if let response = response as? HTTPURLResponse{
                   let result = self.handleNetworkResponse(response)
                   switch result{
                   case .success:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           
                           completion(nil,message)
                       }catch (let missingData){
                           print(missingData)
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }
                   case .conditionFalse:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           completion(message,nil)
                       }catch{
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }

                   case .failure(let networkFailure):
                       print(networkFailure)
                       completion(networkFailure,nil)
                   }
               }
           }
       }
    
    func updateProfile(parameter:[String:String],completion:@escaping(_ error:String?,_ message:String?)->Void){
           generalRouter.request(.updateProfile(params: parameter)) { (data, response, error) in
               if error != nil{
                   completion("Please check your internet connection!",nil)
               }
               if let response = response as? HTTPURLResponse{
                   let result = self.handleNetworkResponse(response)
                   switch result{
                   case .success:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                        let login = Mapper<Login>().map(JSON: json as! [String : Any])
                        UserDefaults.standard.setAddress(address:login?.data?.address ?? "")
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                        
                           
                           completion(nil,message)
                       }catch (let missingData){
                           print(missingData)
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }
                   case .conditionFalse:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           completion(message,nil)
                       }catch{
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }

                   case .failure(let networkFailure):
                       print(networkFailure)
                       completion(networkFailure,nil)
                   }
               }
           }
       }

    
    func getProfile(completion:@escaping(_ error:String?,_ data:UserData?)->Void){
           generalRouter.request(.getProfile) { (data, response, error) in
               if error != nil{
                   completion("Please check your internet connection!",nil)
               }
               if let response = response as? HTTPURLResponse{
                   let result = self.handleNetworkResponse(response)
                   switch result{
                   case .success:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                        let data = Login(map: json as! Map)
                        completion(nil,data?.data)
                       }catch (let missingData){
                           print(missingData)
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }
                   case .conditionFalse:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           completion(message,nil)
                       }catch{
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }

                   case .failure(let networkFailure):
                       print(networkFailure)
                       completion(networkFailure,nil)
                   }
               }
           }
       }

    func signout(completion:@escaping(_ error:String?,_ message:String??)->Void){
           generalRouter.request(.signOut) { (data, response, error) in
               if error != nil{
                   completion("Please check your internet connection!",nil)
               }
               if let response = response as? HTTPURLResponse{
                   let result = self.handleNetworkResponse(response)
                   switch result{
                   case .success:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           
                           completion(nil,message)
                       }catch (let missingData){
                           print(missingData)
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }
                   case .conditionFalse:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           completion(message,nil)
                       }catch{
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }

                   case .failure(let networkFailure):
                       print(networkFailure)
                       completion(networkFailure,nil)
                   }
               }
           }
       }

    
    func updatePicture(image:UIImage,completion:@escaping(_ error:String?,_ message:String??)->Void){
           generalRouter.request(.updateProfilePicture(data: [image])) { (data, response, error) in
               if error != nil{
                   completion("Please check your internet connection!",nil)
               }
               if let response = response as? HTTPURLResponse{
                   let result = self.handleNetworkResponse(response)
                   switch result{
                   case .success:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                       
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           
                           completion(nil,message)
                       }catch (let missingData){
                           print(missingData)
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }
                   case .conditionFalse:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           completion(message,nil)
                       }catch{
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }

                   case .failure(let networkFailure):
                       print(networkFailure)
                       completion(networkFailure,nil)
                   }
               }
           }
       }


    func getOutfits(page:String,limit:String,parameter:[String:String],completion:@escaping(_ error:String?,_ data:[OutfitData]?)->Void){
           generalRouter.request(.getOutfits(page: page, limit: limit, params: parameter)) { (data, response, error) in
               if error != nil{
                   completion("Please check your internet connection!",nil)
               }
               if let response = response as? HTTPURLResponse{
                   let result = self.handleNetworkResponse(response)
                   switch result{
                   case .success:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                            (json as! NSDictionary).value(forKey: "message") as! String
                        let outfits = Mapper<OutfitDetails>().map(JSON: json as! [String:Any])
                        completion(nil,outfits?.outfitData)
                       }catch (let missingData){
                           print(missingData)
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }
                   case .conditionFalse:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           completion(message,nil)
                       }catch{
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }

                   case .failure(let networkFailure):
                       print(networkFailure)
                       completion(networkFailure,nil)
                   }
               }
           }
       }

    func rateOutfits(parameter:[String:String],completion:@escaping(_ error:String?,_ message:String?)->Void){
           generalRouter.request(.rateOutfit(params: parameter)) { (data, response, error) in
               if error != nil{
                   completion("Please check your internet connection!",nil)
               }
               if let response = response as? HTTPURLResponse{
                   let result = self.handleNetworkResponse(response)
                   switch result{
                   case .success:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           
                           completion(nil,message)
                       }catch (let missingData){
                           print(missingData)
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }
                   case .conditionFalse:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           completion(message,nil)
                       }catch{
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }

                   case .failure(let networkFailure):
                       print(networkFailure)
                       completion(networkFailure,nil)
                   }
               }
           }
       }

    func addOutfit(parameter:[String:String],image:UIImage,completion:@escaping(_ error:String?,_ message:String??)->Void){
        generalRouter.request(.addOutfit(params: parameter,data:[image])) { (data, response, error) in
               if error != nil{
                   completion("Please check your internet connection!",nil)
               }
               if let response = response as? HTTPURLResponse{
                   let result = self.handleNetworkResponse(response)
                   switch result{
                   case .success:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           
                           completion(nil,message)
                       }catch (let missingData){
                           print(missingData)
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }
                   case .conditionFalse:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           completion(message,nil)
                       }catch{
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }

                   case .failure(let networkFailure):
                       print(networkFailure)
                       completion(networkFailure,nil)
                   }
               }
           }
       }
    

    func getUserOutfit(page:String,limit:String,completion:@escaping(_ error:String?,_ outifits:[UserOutfits]?)->Void){
           generalRouter.request(.getUserOutfits(page: page, limit: limit)) { (data, response, error) in
               if error != nil{
                   completion("Please check your internet connection!",nil)
               }
               if let response = response as? HTTPURLResponse{
                   let result = self.handleNetworkResponse(response)
                   switch result{
                   case .success:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                          
                        let outfits = Mapper<UserOutfitDetails>().map(JSON: json as! [String:Any])
                        Utility.shared.saveOutfits(outfits: outfits?.outfitData ?? [])
                        completion(nil,outfits?.outfitData)
                       }catch (let missingData){
                           print(missingData)
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }
                   case .conditionFalse:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           completion(message,nil)
                       }catch{
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }

                   case .failure(let networkFailure):
                       print(networkFailure)
                       completion(networkFailure,nil)
                   }
               }
           }
       }

    func deleteOutfit(id:String,completion:@escaping(_ error:String?,_ message:String??)->Void){
           generalRouter.request(.deleteOutfit(id: id)) { (data, response, error) in
               if error != nil{
                   completion("Please check your internet connection!",nil)
               }
               if let response = response as? HTTPURLResponse{
                   let result = self.handleNetworkResponse(response)
                   switch result{
                   case .success:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           
                           completion(nil,message)
                       }catch (let missingData){
                           print(missingData)
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }
                   case .conditionFalse:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           completion(message,nil)
                       }catch{
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }

                   case .failure(let networkFailure):
                       print(networkFailure)
                       completion(networkFailure,nil)
                   }
               }
           }
       }
    
    func editOutfitWithoutImage(id:String,parameter:[String:String],completion:@escaping(_ error:String?,_ message:String??)->Void){
        generalRouter.request(.editOutfitWithoutImage(id: id, params: parameter)) { (data, response, error) in
               if error != nil{
                   completion("Please check your internet connection!",nil)
               }
               if let response = response as? HTTPURLResponse{
                   let result = self.handleNetworkResponse(response)
                   switch result{
                   case .success:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           
                           completion(nil,message)
                       }catch (let missingData){
                           print(missingData)
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }
                   case .conditionFalse:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           completion(message,nil)
                       }catch{
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }

                   case .failure(let networkFailure):
                       print(networkFailure)
                       completion(networkFailure,nil)
                   }
               }
           }
       }

    
    func editOutfit(id:String,parameter:[String:String],image:UIImage,completion:@escaping(_ error:String?,_ message:String??)->Void){
        generalRouter.request(.editOutfit(id: id, params: parameter,data:[image])) { (data, response, error) in
               if error != nil{
                   completion("Please check your internet connection!",nil)
               }
               if let response = response as? HTTPURLResponse{
                   let result = self.handleNetworkResponse(response)
                   switch result{
                   case .success:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           
                           completion(nil,message)
                       }catch (let missingData){
                           print(missingData)
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }
                   case .conditionFalse:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           completion(message,nil)
                       }catch{
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }

                   case .failure(let networkFailure):
                       print(networkFailure)
                       completion(networkFailure,nil)
                   }
               }
           }
       }

    func deleteEvent(id:String,completion:@escaping(_ error:String?,_ message:String??)->Void){
           generalRouter.request(.deleteEvent(id: id)) { (data, response, error) in
               if error != nil{
                   completion("Please check your internet connection!",nil)
               }
               if let response = response as? HTTPURLResponse{
                   let result = self.handleNetworkResponse(response)
                   switch result{
                   case .success:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           
                           completion(nil,message)
                       }catch (let missingData){
                           print(missingData)
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }
                   case .conditionFalse:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           completion(message,nil)
                       }catch{
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }

                   case .failure(let networkFailure):
                       print(networkFailure)
                       completion(networkFailure,nil)
                   }
               }
           }
       }

    
 func addEvent(parameter:[String:String],completion:@escaping(_ error:String?,_ message:String??)->Void){
        generalRouter.request(.addEvent(params: parameter)) { (data, response, error) in
            if error != nil{
                completion("Please check your internet connection!",nil)
            }
            if let response = response as? HTTPURLResponse{
                let result = self.handleNetworkResponse(response)
                switch result{
                case .success:
                    guard let responseData = data else{
                        completion(NetworkResponse.noData.rawValue,nil)
                        return
                    }
                    do{
                        let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                        print(json)
                        let message = (json as! NSDictionary).value(forKey: "message") as! String
                        
                        completion(nil,message)
                    }catch (let missingData){
                        print(missingData)
                        completion(NetworkResponse.unableToDecode.rawValue,nil)
                    }
                case .conditionFalse:
                    guard let responseData = data else{
                        completion(NetworkResponse.noData.rawValue,nil)
                        return
                    }
                    do{
                        let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                        print(json)
                        let message = (json as! NSDictionary).value(forKey: "message") as! String
                        completion(message,nil)
                    }catch{
                        completion(NetworkResponse.unableToDecode.rawValue,nil)
                    }

                case .failure(let networkFailure):
                    print(networkFailure)
                    completion(networkFailure,nil)
                }
            }
        }
    }

    func getEvent(page:String,limit:String,parameter:[String:String],completion:@escaping(_ error:String?,_ events:[UserEvents]?)->Void){
           generalRouter.request(.getEvent(page: page, limit: limit, params: parameter)) { (data, response, error) in
               if error != nil{
                   completion("Please check your internet connection!",nil)
               }
               if let response = response as? HTTPURLResponse{
                   let result = self.handleNetworkResponse(response)
                   switch result{
                   case .success:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                        let events = Mapper<EventDetails>().map(JSON: json as! [String:Any])
                           
                        completion(nil,events?.eventDetails)
                       }catch (let missingData){
                           print(missingData)
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }
                   case .conditionFalse:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           completion(message,nil)
                       }catch{
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }

                   case .failure(let networkFailure):
                       print(networkFailure)
                       completion(networkFailure,nil)
                   }
               }
           }
       }
    
    func userEvents(page:String,limit:String,parameter:[String:String],completion:@escaping(_ error:String?,_ events:[UserEvents]?)->Void){
              generalRouter.request(.userEvent(page: page, limit: limit)) { (data, response, error) in
                  if error != nil{
                      completion("Please check your internet connection!",nil)
                  }
                  if let response = response as? HTTPURLResponse{
                      let result = self.handleNetworkResponse(response)
                      switch result{
                      case .success:
                          guard let responseData = data else{
                              completion(NetworkResponse.noData.rawValue,nil)
                              return
                          }
                          do{
                              let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                              print(json)
                           let events = Mapper<EventDetails>().map(JSON: json as! [String:Any])
                              
                           completion(nil,events?.eventDetails)
                          }catch (let missingData){
                              print(missingData)
                              completion(NetworkResponse.unableToDecode.rawValue,nil)
                          }
                      case .conditionFalse:
                          guard let responseData = data else{
                              completion(NetworkResponse.noData.rawValue,nil)
                              return
                          }
                          do{
                              let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                              print(json)
                              let message = (json as! NSDictionary).value(forKey: "message") as! String
                              completion(message,nil)
                          }catch{
                              completion(NetworkResponse.unableToDecode.rawValue,nil)
                          }

                      case .failure(let networkFailure):
                          print(networkFailure)
                          completion(networkFailure,nil)
                      }
                  }
              }
          }
    
    func editEvent(id:String,parameter:[String:String],completion:@escaping(_ error:String?,_ message:String??)->Void){
           generalRouter.request(.editEvent(id: id, params: parameter)) { (data, response, error) in
               if error != nil{
                   completion("Please check your internet connection!",nil)
               }
               if let response = response as? HTTPURLResponse{
                   let result = self.handleNetworkResponse(response)
                   switch result{
                   case .success:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           
                           completion(nil,message)
                       }catch (let missingData){
                           print(missingData)
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }
                   case .conditionFalse:
                       guard let responseData = data else{
                           completion(NetworkResponse.noData.rawValue,nil)
                           return
                       }
                       do{
                           let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                           print(json)
                           let message = (json as! NSDictionary).value(forKey: "message") as! String
                           completion(message,nil)
                       }catch{
                           completion(NetworkResponse.unableToDecode.rawValue,nil)
                       }

                   case .failure(let networkFailure):
                       print(networkFailure)
                       completion(networkFailure,nil)
                   }
               }
           }
       }


    
    //MARK: Method to handle Network Response
    fileprivate func handleNetworkResponse(_ response:HTTPURLResponse) -> Result<String>{
        switch response.statusCode{
        case 200...299: return .success
        //case 401: return .notAuthenticated
        case 400...500: return .conditionFalse
        case 501...599: return .failure(NetworkResponse.badRequest.rawValue)
        case 600: return .failure(NetworkResponse.outdated.rawValue)
        default: return .failure(NetworkResponse.failed.rawValue)
        }
    }
}


