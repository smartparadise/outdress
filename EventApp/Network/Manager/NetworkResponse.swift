//
//  AppDelegate.swift
//  EventApp
//
//  Created by Pankaj Sharma on 07/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//
import Foundation

public enum NetworkResponse:String{
    case success
    case authenticationError = "You need to be authenticated first!"
    case badRequest = "Bad request"
    case outdated = "The url you request is outdated"
    case failed = "Network request failed"
    case noData = "Response return with no data to decode"
    case unableToDecode = "we are not able to decode the response"
}

public enum Result<String>{
    case success
    case conditionFalse
    case failure(String)
}
