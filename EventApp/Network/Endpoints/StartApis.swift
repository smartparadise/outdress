//
//  AppDelegate.swift
//  EventApp
//
//  Created by Pankaj Sharma on 07/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//
import Foundation

public enum StartApis:EndpointTypes{
    
    case login(params:[String:String])
    case signup(params:[String:String])
    case forgot(params:[String:String])
    
    var baseUrl: URL{
        guard let url = URL(string: ApiUrls.baseUrl) else{fatalError("base url couln't be configured")}
        return url
    }
    
    var path: String{
        switch self {
        case .login:
            return ApiUrls.login
        case .signup:
            return ApiUrls.signUp
        case .forgot:
            return ApiUrls.forgotPassword
        }
    }
    
    var httpMethod: HTTPMethod{
        switch self {
        case .login:
            return .post
        case .signup:
            return .post
        case .forgot:
            return .post
        }
        
    }
    
    var task: HTTPTask{
        switch self{
        case .signup(let params):
            return .requestParamtersAndHeaders(bodyParameters: params, urlParameters: nil, additionalHeaders: headers)
        case .login(let params):
            return .requestParamtersAndHeaders(bodyParameters: params, urlParameters: nil, additionalHeaders: headers)
        case .forgot(let params):
            return .requestParamtersAndHeaders(bodyParameters: params, urlParameters: nil, additionalHeaders: headers)
        }
    }
    
    var headers: HTTPHeaders?{
        return AuthHeader.makeAuth()
    }
}

