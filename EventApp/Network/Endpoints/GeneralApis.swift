//
//  GeneralApis.swift
//  EventApp
//
//  Created by Pankaj Sharma on 28/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import Foundation


public enum GeneralApis:EndpointTypes{
    
    case changePassword(params:Parameters)
    case changeGender(params:Parameters)
    case updateProfile(params:Parameters)
    case getProfile
    case signOut
    case updateProfilePicture(data:MultipartData)
    case getOutfits(page:String,limit:String,params:Parameters)
    case rateOutfit(params:Parameters)
    
    case addOutfit(params:Parameters,data:MultipartData)
    case getUserOutfits(page:String,limit:String)
    case deleteOutfit(id:String)
    case editOutfit(id:String,params:Parameters,data:MultipartData)
    case editOutfitWithoutImage(id:String,params:Parameters)
    
    case deleteEvent(id:String)
    case addEvent(params:Parameters)
    case getEvent(page:String,limit:String,params:Parameters)
    case editEvent(id:String,params:Parameters)
    case userEvent(page:String,limit:String)
    
    
    var baseUrl: URL{
        guard let url = URL(string: ApiUrls.baseUrl) else{fatalError("base url couln't be configured")}
        return url
    }
    
    var path: String{
        switch self {
        case .changePassword:
            return ApiUrls.changePassword
        case .changeGender:
            return ApiUrls.changeGender
        case .updateProfile:
            return ApiUrls.updateProfile
        case .getProfile:
            return ApiUrls.getProfile
        case .signOut:
            return ApiUrls.signOut
        case .updateProfilePicture:
            return ApiUrls.updateProfilePicture
        case .getOutfits(let page,let limit, _):
            return ApiUrls.getOutfits + "/" + page + "/" + limit
        case .rateOutfit:
            return ApiUrls.rateOutfit
        case .addOutfit:
            return ApiUrls.addOutfit
        case .getUserOutfits(let page, let limit):
            return ApiUrls.getUserOutfit + "/" + page + "/" + limit
        case .deleteOutfit(let id):
            return ApiUrls.deleteUserOutfit + "/" + id
        case .editOutfit(let id,let _, _):
            return ApiUrls.editOutfit + "/" + id
        case .editOutfitWithoutImage(let id,let _):
            return ApiUrls.editOutfit + "/" + id
        case .deleteEvent(let id):
            return ApiUrls.editEvent + "/" + id
        case .addEvent:
            return ApiUrls.addEvent
        case .getEvent(let page, let limit, _),.userEvent(let page, let limit):
            return ApiUrls.getEvents + "/" + page + "/" + limit
        case .editEvent(let id, _):
            return ApiUrls.editEvent + "/" + id
         }
    }
    
    var httpMethod: HTTPMethod{
        switch self {
         case .changePassword:
            return .put
           case .changeGender:
            return .post
           case .updateProfile:
            return .post
           case .getProfile:
            return .get
           case .signOut:
            return .put
           case .updateProfilePicture:
            return .post
           case .getOutfits:
            return .get
           case .rateOutfit:
            return .post
           case .addOutfit:
            return .post
           case .getUserOutfits:
            return .get
           case .deleteOutfit:
            return .delete
           case .editOutfit:
            return .post
           case .editOutfitWithoutImage:
                       return .post
           case .deleteEvent:
            return .delete
           case .addEvent:
            return .post
        case .getEvent,.userEvent:
            return .get
           case .editEvent:
            return .post
        }
        
    }
    
    var task: HTTPTask{
         switch self {
               case .changePassword(let params):
                return .requestParamtersAndHeaders(bodyParameters: params, urlParameters: nil, additionalHeaders: headers)
               case .changeGender(let params):
                   return .requestParamtersAndHeaders(bodyParameters: params, urlParameters: nil, additionalHeaders: headers)
               case .updateProfile(let params):
                   return .requestParamtersAndHeaders(bodyParameters: params, urlParameters: nil, additionalHeaders: headers)
               case .getProfile:
                    return .requestParamtersAndHeaders(bodyParameters: nil, urlParameters: nil, additionalHeaders: headers)
               case .signOut:
                   return .requestParamtersAndHeaders(bodyParameters: nil, urlParameters: nil, additionalHeaders: headers)
               case .updateProfilePicture(let data):
                return .requestMultipartPost(bodyParameters: nil, additionalHeaders: headers, data: data, key: "profile_picture")
         case .getOutfits( _, _,let params):
                   return .requestParamtersAndHeaders(bodyParameters: nil, urlParameters: params, additionalHeaders: headers)
               case .rateOutfit(let params):
                   return .requestParamtersAndHeaders(bodyParameters: params, urlParameters: nil, additionalHeaders: headers)
               case .addOutfit(let params,let data):
                    return .requestMultipartPost(bodyParameters: params, additionalHeaders: headers, data: data, key: "picture")
//            case .addOutfit(let params,_):
  //                             return .requestParamtersAndHeaders(bodyParameters: params, urlParameters: nil, additionalHeaders: headers)
         case .getUserOutfits( _,  _):
                   return .requestParamtersAndHeaders(bodyParameters: nil, urlParameters: nil, additionalHeaders: headers)
               case .deleteOutfit( _):
                   return .requestParamtersAndHeaders(bodyParameters: nil, urlParameters: nil, additionalHeaders: headers)
               case .editOutfit( _,let params,let data):
                   return .requestMultipartPost(bodyParameters: params, additionalHeaders: headers, data: data, key: "picture")
            case .editOutfitWithoutImage( _,let params):
                return .requestParamtersAndHeaders(bodyParameters: params, urlParameters: nil, additionalHeaders: headers)
               case .deleteEvent( _):
                    return .requestParamtersAndHeaders(bodyParameters: nil, urlParameters: nil, additionalHeaders: headers)
               case .addEvent(let params):
                    return .requestParamtersAndHeaders(bodyParameters: params, urlParameters: nil, additionalHeaders: headers)
               case .getEvent( _,  _,let params):
                    return .requestParamtersAndHeaders(bodyParameters: nil, urlParameters: params, additionalHeaders: headers)
         case .userEvent:
            return .requestParamtersAndHeaders(bodyParameters: nil, urlParameters: nil, additionalHeaders: headers)
               case .editEvent( _,let params):
                    return .requestParamtersAndHeaders(bodyParameters: params, urlParameters: nil, additionalHeaders: headers)
                }
    }
    
    var headers: HTTPHeaders?{
        return UserHeaders.makeAuth()
    }
}
