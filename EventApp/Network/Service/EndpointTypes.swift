//
//  AppDelegate.swift
//  EventApp
//
//  Created by Pankaj Sharma on 07/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import Foundation

protocol EndpointTypes {
    var baseUrl:URL {get}
    var path:String {get}
    var httpMethod: HTTPMethod{get}
    var task:HTTPTask{get}
    var headers:HTTPHeaders?{get}
}
