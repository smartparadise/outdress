//
//  AppDelegate.swift
//  EventApp
//
//  Created by Pankaj Sharma on 07/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import Foundation

public enum HTTPMethod:String{
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}
