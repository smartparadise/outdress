//
//  AppDelegate.swift
//  EventApp
//
//  Created by Pankaj Sharma on 07/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import Foundation

public typealias NetworkRouterCompletion = (_ data:Data?,_ response:URLResponse?,_ error:Error?) ->()

protocol NetworkRouter:class{
    associatedtype EndPoint:EndpointTypes
    func request(_ route:EndPoint,completion:@escaping NetworkRouterCompletion)
    func cancel()
}
