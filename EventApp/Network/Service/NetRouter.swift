//
//  AppDelegate.swift
//  EventApp
//
//  Created by Pankaj Sharma on 07/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import Foundation

struct ReachabilityError:LocalizedError{
    var title: String?
    var code: Int
    var errorDescription: String? { return _description }
    var failureReason: String? { return _description }
    
    private var _description: String
    
    init(title: String?, description: String, code: Int) {
        self.title = title ?? "Error"
        self._description = description
        self.code = code
    }
}

class NetRouter<EndPoint:EndpointTypes>:NetworkRouter{

    private var task:URLSessionTask?
    
    //MARK: - Method to make Request to Network
    func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion) {
        let session = URLSession.shared
        if Reachability.isConnectedToNetwork(){
            do{
                let request = try self.buildRequest(from: route)
                
                task = session.dataTask(with: request, completionHandler: { (data, response, error) in
                    completion(data,response,error)
                })
            }catch{
                completion(nil,nil,error)
            }
            self.task?.resume()
        }else{
            let mError = ReachabilityError(title: "No Internet Connection", description: "Internet connection is off.", code: 586)
            completion(nil,nil,mError)
        }
    }
    
   
    //MARK: - Method to Build Request
    fileprivate func buildRequest(from route:EndPoint) throws -> URLRequest{
        var request  = URLRequest(url: route.baseUrl.appendingPathComponent(route.path), cachePolicy: .reloadIgnoringLocalAndRemoteCacheData)
        request.httpMethod = route.httpMethod.rawValue
        do{
            switch route.task {
            case .request:
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
            case .requestParameters(let bodyParameters , let urlParameters):
                try self.configureParameters(bodyParameters:bodyParameters,urlParameters:urlParameters,request:&request)
                
            case .requestParamtersAndHeaders(let bodyParameters, let urlParameters,let additionalHeaders):
                self.addAdditionalHeaders(additionalHeaders,request:&request)
                try self.configureParameters(bodyParameters:bodyParameters,urlParameters:urlParameters,request:&request)
           
            case .requestMultipartPost(let bodyParameters, let additionalHeaders, data: let multipartData,key: let key):
                self.addAdditionalHeaders(additionalHeaders, request: &request)
                try self.addMultipartData(bodyParameters, multipartData, request: &request,key:key)
                
            case .uploadData(let additionalHeaders, data: let multipartData,key:let key):
                self.addAdditionalHeaders(additionalHeaders, request: &request)
                try self.addMultipartWithoutParams(multipartData, request: &request,key:key)
            
            }
            return request
        }catch{
            throw error
        }
    }
    
    //MARK: - Method to Configure Parameters
    fileprivate func configureParameters(bodyParameters:Parameters?,urlParameters:Parameters?,request:inout URLRequest) throws{
        do{
            if let bodyParameters = bodyParameters{
                try JSONParameterEncoder.encode(urlRequest: &request, with: bodyParameters)
            }
            if let urlParameters = urlParameters{
                try URLParameterEncoder.encode(urlRequest: &request, with: urlParameters)
            }
        }catch{
            throw error
        }
    }
    
    //MARK: - Method to add Headers
    fileprivate func addAdditionalHeaders(_ additionalHeaders:HTTPHeaders?,request:inout URLRequest){
        guard let headers = additionalHeaders else{return}
        for (key,value) in headers{
            request.setValue(value, forHTTPHeaderField: key)
        }
    }
    
    //MARK: - Method to add Images Data for Multipart
    fileprivate func addMultipartData(_ parameters:Parameters?,_ data:MultipartData?,request:inout URLRequest,key:String) throws{
        do{
            if let multipartData = data,let parameters = parameters{
                try MultipartEncoder.encode(urlRequest: &request, parameters: parameters, with: multipartData,key:key)
            }
        }catch{
            throw error
        }
    }
    
    //MARK: - Method to Upload Data Without Parameters for Multipart
    fileprivate func addMultipartWithoutParams(_ data:MultipartData?,request:inout URLRequest,key:String) throws{
        do{
            if let multipartData = data{
                try MultipartEncoder.encodeData(urlRequest: &request, with: multipartData,key:key)
            }
        }catch{
            throw error
        }
    }
    
    //MARK: - Method to cancel task
    func cancel() {
        self.task?.cancel()
    }
}
