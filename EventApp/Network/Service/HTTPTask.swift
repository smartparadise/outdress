//
//  AppDelegate.swift
//  EventApp
//
//  Created by Pankaj Sharma on 07/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import Foundation
import UIKit

public typealias HTTPHeaders = [String:String]

public enum HTTPTask{
    case request
    
    case requestParameters(bodyParameters:Parameters?,urlParameters:Parameters?)
    
    case requestParamtersAndHeaders(bodyParameters:Parameters?,urlParameters:Parameters?,additionalHeaders:HTTPHeaders?)
    
    case requestMultipartPost(bodyParameters:Parameters?,additionalHeaders:HTTPHeaders?,data:MultipartData?,key:String)
    
    case uploadData(additionalHeaders:HTTPHeaders?,data:MultipartData?,key:String)
    
    
}
