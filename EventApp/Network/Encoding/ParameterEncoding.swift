//
//  AppDelegate.swift
//  EventApp
//
//  Created by Pankaj Sharma on 07/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import Foundation
import UIKit

//Aliases declared of specific types
public typealias Parameters = [String:Any]
public typealias MultipartData = [Any]

//This Protocol will be comfored by different parameter encoder like for url or json encoder
protocol ParameterEncoder {
    static func encode(urlRequest:inout URLRequest,with parameters:Parameters) throws
}
//This Protocol will be comfored by multipart request encoder
protocol MultiEncoder{
    static func encode(urlRequest:inout URLRequest ,parameters:Parameters,with data:MultipartData,key:String) throws
    static func encodeData(urlRequest:inout URLRequest, with data:MultipartData,key:String) throws
}

//Custom Errors thrown while decoding errors
public enum NetworkError:String,Error{
    case parametersNil = "Parameters were nil"
    case encodingFailed = "Parameters encoding failed"
    case missingURL = "url is missing"
    case audioConversionFailed = "unable to convert audio to data"
    case imageConvertFailed = "unable to convert images to data"
    case networkError = "No Internet Connection."
}
