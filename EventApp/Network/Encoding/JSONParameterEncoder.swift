//
//  AppDelegate.swift
//  EventApp
//
//  Created by Pankaj Sharma on 07/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//
import Foundation

//Encoder to encode the parameters as body in api call 
public struct JSONParameterEncoder:ParameterEncoder{
    
    public static func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws {
        do{
            print(parameters)
           // let jsonAsData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            let jsonAsData = parameters.percentEscaped().data(using: .utf8)
            urlRequest.httpBody = jsonAsData

            if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil{
              //  urlRequest.addValue("application/json", forHTTPHeaderField: "content-type")
                urlRequest.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "content-type")
            }
        }catch{
            throw NetworkError.encodingFailed
        }
    }
}
