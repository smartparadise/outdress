//
//  AppDelegate.swift
//  EventApp
//
//  Created by Pankaj Sharma on 07/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//
import Foundation
import UIKit
import MobileCoreServices

//Encoder to encode parameters and file data i.e images in url as multipart form data
public struct MultipartEncoder:MultiEncoder{
    
    static func encode(urlRequest: inout URLRequest,parameters:Parameters, with data: MultipartData,key:String) throws {
        
        let boundary = UUID().uuidString
        
        do {
            guard let bodyData = try createMediaBody(params: parameters, data: data, boundary: boundary,key:key)else{
                throw NetworkError.imageConvertFailed

            }
            urlRequest.httpBody = bodyData //try createImageBody(params:parameters,data: data, boundary: boundary)
        }catch{
            throw NetworkError.imageConvertFailed
        }
        
        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil  {
           urlRequest.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        }
    }
    
    static func encodeData(urlRequest: inout URLRequest, with data: MultipartData,key:String) throws {
        let boundary = UUID().uuidString
        
        do {
            guard let bodyData = try createMediaBody(params: [:], data: data, boundary: boundary,key:key)else{
                throw NetworkError.imageConvertFailed
            }
            urlRequest.httpBody = bodyData //try createImageBody(params:[:],data: data, boundary: boundary)
        }catch{
            throw NetworkError.audioConversionFailed
        }

        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil  {
            urlRequest.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        }
    }
    
    //Method to create body with parameters and images
    static func createMediaBody(params:Parameters,data:MultipartData, boundary:String,key:String)throws -> Data? {
        var body = Data()
        var i = 0;
        //let pictureKey = "pictures[]"
        let mediaKey = key
        for (key,value) in params{
            if(value is String){
                body.append(string:"--\(boundary)\r\n")
                body.append(string:"Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.append(string:"\(value)\r\n")
            }
        }

        for datum in data {
            if datum is UIImage{
                let image = datum as! UIImage
                print(image)
//                guard let compressedData = image.compressTo(3) else {
//                    return nil
//                }
                let compressedData = image.jpegData(compressionQuality: 1)
                let filename = "image\(i).jpg"
               // let data = compressedImage.jpegData(compressionQuality: 1)
                let mimetype = mimeTypeForPath(path: filename)

                body.append(string:"--\(boundary)\r\n")
                body.append(string:"Content-Disposition: form-data; name=\"\(mediaKey)\"; filename=\"\(filename)\"\r\n")
                body.append(string:"Content-Type: \(mimetype)\r\n\r\n")
                body.append(compressedData!)
                body.append(string:"\r\n")
                i += 1;
            }else if datum is URL && key == "media[]"{

                let url = datum as! URL

                let filename = "advertisement.mov"
                var data:Data!
                do{
                    data = try Data(contentsOf: url, options: .alwaysMapped)
                    let mimetype = "video/mov"

                    body.append(string:"--\(boundary)\r\n")
                    body.append(string:"Content-Disposition: form-data; name=\"\(mediaKey)\"; filename=\"\(filename)\"\r\n")
                    body.append(string:"Content-Type: \(mimetype)\r\n\r\n")
                    body.append(data!)
                    body.append(string:"\r\n")
                }catch{
                    print("Data conversion error")
                }
            }else if datum is URL && key == "media"{
                let url = datum as! URL
                let filename = "audio.mkv"
                var data:Data!
                do{
                    data = try Data(contentsOf: url)
                    let mimetype = mimeTypeForPath(path: filename)

                    body.append(string:"--\(boundary)\r\n")
                    body.append(string:"Content-Disposition: form-data; name=\"\(mediaKey)\"; filename=\"\(filename)\"\r\n")
                    body.append(string:"Content-Type: \(mimetype)\r\n\r\n")
                    body.append(data!)
                    body.append(string:"\r\n")
                }catch{
                    print("Data conversion error")
                }
            }
        }
        body.append(string:"--\(boundary)--\r\n")
        return body
    }

static func mimeTypeForPath(path: String) -> String {
    let pathExtension = URL(string: path)?.pathExtension
    var stringMimeType = "application/octet-stream";

    if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as CFString, nil)?.takeRetainedValue() {
        if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
            stringMimeType = mimetype as NSString as String
        }
    }
    return stringMimeType;
    }
}

