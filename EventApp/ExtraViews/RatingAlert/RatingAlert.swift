//
//  RatingAlert.swift
//  EventApp
//
//  Created by Pankaj Sharma on 22/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit

protocol RatingDelegate{
    func closeTapped()
    func submitTapped(comment:String)
}

class RatingAlert: UIView {
    
    @IBOutlet var contentView: UIView!
    var delegate:RatingDelegate!
    
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var textBackView: UIView!
    @IBOutlet weak var commentTV: UITextView!
    
    init(frame:CGRect,delegate:RatingDelegate) {
        super.init(frame: frame)
        self.delegate = delegate
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    func commonInit(){
        Bundle.main.loadNibNamed("RatingAlert", owner: self, options: nil)
        self.contentView.frame = self.bounds
        self.contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.addSubview(contentView)
        Utility.makeBorder(mView: textBackView, radius: 30, width: 1, color: .lightGray)
        commentTV.delegate = self
    }
    
    @IBAction func alertCloseTapped(_ sender: Any) {
        self.removeFromSuperview()
        delegate.closeTapped()
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        self.removeFromSuperview()
        delegate.submitTapped(comment:commentTV.text!)
    }
}

extension RatingAlert:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == commentTV{
            if textView.textColor == UIColor.lightGray{
                textView.text = ""
                textView.textColor = UIColor.black
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == commentTV{
            if textView.text == ""{
                textView.textColor = UIColor.lightGray
                textView.text = "Write here"
            }
        }
    }
}
