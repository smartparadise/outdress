//
//  ColorPickerCell.swift
//  EventApp
//
//  Created by Pankaj Sharma on 15/02/20.
//  Copyright © 2020 Pankaj Sharma. All rights reserved.
//

import UIKit

class ColorPickerCell: UICollectionViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var viewColor: UIView!
    
    @IBOutlet weak var constraintViewWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintViewHeight: NSLayoutConstraint!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        constraintViewWidth.constant = UIScreen.main.bounds.size.width/7.3
        constraintViewHeight.constant = UIScreen.main.bounds.size.width/7.3
        viewColor.layer.cornerRadius = constraintViewWidth.constant/2
        viewColor.layer.masksToBounds = true
               
    }

}
