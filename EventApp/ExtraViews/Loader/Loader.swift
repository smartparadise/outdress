//
//  BillCodeCell.swift
//  EAGLTECH
//
//  Created by Pankaj Sharma on 12/12/19.
//  Copyright © 2019 Pankaj Sharma. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class Loader: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var loader: NVActivityIndicatorView!
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit(){
        Bundle.main.loadNibNamed("Loader", owner: self, options: nil)
        self.contentView.frame = self.frame
        self.contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.addSubview(self.contentView)
        loader.startAnimating()
    }
}
